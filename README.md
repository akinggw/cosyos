![logo](images/logo.png) _温馨舒适的操作系统、零中断延迟的操作系统_ :grey_exclamation: 

![Author](https://img.shields.io/badge/Author-迟凯峰-red?style=flat-square)
![languge](https://img.shields.io/badge/languge-C、Asm-blue)
![license](https://img.shields.io/badge/license-Apache2.0-brightgreen.svg)
![version](https://img.shields.io/badge/version-2.3.8-green)
[![star](https://gitee.com/cosyos/cosyos/badge/star.svg?theme=dark)](https://gitee.com/cosyos/cosyos/stargazers)
[![fork](https://gitee.com/cosyos/cosyos/badge/fork.svg?theme=dark)](https://gitee.com/cosyos/cosyos/members)

### 序
纵观天下之RTOS，普遍都存在关闭总中断的情况，导致中断不能被实时处理。中断才是最需要实时处理的，而不是任务，中断的实时性问题不解决，任务调度的再及时、切换的再快也是缘木求鱼。从这个角度来说，Keil RTX是非常优秀的，因为它实现了Cortex-M3/M4内核全局不关中断，然而对于其它内核来说却没有这个待遇。<br>
设计一款能够实现所有内核全局不关中断的RTOS成为了作者的奋斗目标，否则在如今RTOS遍地开花的情况下，再设计千篇一律的RTOS还有什么意义呢？

### 简介
CosyOS始创于2021年，并于2022年底完成kernel的第一个版本V1.0.0。作者以“突破创新”为理念，“简单易用”为原则，经过不懈的努力，终于大获成功。CosyOS的最大亮点是实现了**所有内核全局不关中断（零中断延迟）**。除此之外，一些传统技术均实现了突破与创新。如消息邮箱，支持随意定义数据类型，丰富了邮件的形式；事件标志组，支持定义标志位，方便了标志组的应用。另外，CosyOS还独创了一些新技术，如私信等，会陆续向大家介绍... ...<br>

下面，让我们来初步体验一下CosyOS的易用性。

### 初体验

 **CosyOS一步创建任务示例：** 

| 任务名称       | 任务优先级 | 任务栈大小 | 安全运行时 | 私信    |
|------------|-------|-------|-------|-------|
| demo1_task | 1级    | 128字节 | 0，无限长 | 0，无私信 |
| demo2_task | 2级    | 256字节 | 9个时间片 | 3个参数  |

注1：**安全运行时**是CosyOS的安全关键技术，可防止某任务长期独占或超时使用处理器。<br>
注2：**私信**是CosyOS独创的一种任务间通讯方式，可用来实现信号、事件、消息等功能。

```c
# 创建demo1_task
uCreateTask(demo1_task, 1, 128, 0, 0)
{
    ... ...
    uSendDM(demo2_task) "hello", 999, 3.14); // 发送私信至demo2_task（C89Mode）
    uSendDM(demo2_task, "hello", 999, 3.14); // 发送私信至demo2_task（C99Mode）
    ... ...
    uEndTasking; // 所有任务的最后一句代码
}
```

```c
# 创建demo2_task
uCreateTask(demo2_task, 2, 256, 9, 3)(char *p, int a, float b)
{
    ... ...
    if(uRecvDM(500)) // 接收私信（超时时间为500个滴答周期，返回值为真则接收成功）
    {
        /* 使用私信（读取p、a、b）*/
    }
    ... ...
    uEndTasking;
}
```

```c
# 启动钩子
void start_hook(void)
{
    uStartTask(demo1_task, 0); // 启动demo1_task并置任务的初始状态为就绪状态
    uStartTask(demo2_task, 1); // 启动demo2_task并置任务的初始状态为挂起状态
}
```

您有没有眼前一亮呢？CosyOS创建一个任务竟如此简单，通过调用API“uCreateTask”，输入各项参数并直接写任务代码即可（已集成用于任务循环的while(1)，用户不必再写循环）。下一步就是在启动钩子中启动任务，任务便可参与调度并运行了。CosyOS还开创性的把任务形参用做私信，私信参数（数量、名称、类型）可随意定义，与普通函数定义形参如出一辙。其它应用也都有着异曲同工之妙，即无论做什么事，都尽可能做到简化流程一步完成，最大程度的降低开发者的工作量，给开发者创造一个温馨舒适的开发环境。

### 突破创新
* 开创性的实现了**所有内核全局不关中断（零中断延迟）**，保证了中断中用户代码的实时性
* 独创的**私信**（任务形参），参数可随意定义，使用极为灵活，是任务间通讯的利器
* 独创的**软件RTC**，支持设置时间和获取时间，可替代硬件RTC
* 80251内核支持独创的**虚拟双栈指针技术**，使任务的切换效率等同于Cortex-M
* **定时中断任务/钩子**、**定时查询任务/钩子**，针对于嵌入式开发中最典型的应用
* **消息邮箱**，每个邮箱在创建时，都可定义属于自己的数据类型，极大的丰富了邮件的形式，方便了任务间消息的传递
* **消息队列**，同时支持静态创建和动态创建，传输模式支持FIFO、LIFO，采用高效的指针引用方式
* **事件标志组**，声明标志组的同时定义标志位，不同标志组的标志位可以重名，对标志组和标志位的访问通过组名和位名来实现，极大的方便了标志组的应用
* **全局变量访问**，通过调用API可实现在任意任务和中断中对全局变量的安全访问，而不必担心重入的发生
* **安全关键技术**，拥有多项安全关键技术，如中断异步服务空间隔离、安全运行时等，可靠性高
* **任务栈监控**，拥有多项任务栈监控措施，如每调度监控、线程入口监控等，可提前预判任务栈溢出的风险

### 因循守旧
* 完全开源的免版税、确定性的RTOS
* 任务调度支持抢占式调度、时间片轮转调度
* 用户任务数量不限，且每个任务都可以有255级优先级（0~254）
* 简洁高效的代码，极低的硬件资源占用，使CosyOS可轻松应用于各种小型MCU
* 任务管理器，可实时监控各任务的运行，便于开发者急时发现设计中存在的潜在问题<br>
![任务管理器](images/taskmgr_0.png "任务管理器")<br>
—————————— CosyOS-任务管理器 ——————————

### 支持内核
CosyOS现支持8051、80251、Cortex-M等内核，未来会陆续添加对其它内核的支持。<br>

### 编译环境
CosyOS是在keil C51、C251、Arm编译器下开发的，对其支持最好。未来，将会陆续优化调整对其它编译器的支持。

### 文件说明
| 名称     | 描述                           |
|--------|------------------------------|
| System | CosyOS的内核文件                  |
| Config | CosyOS的配置文件，包括系统配置文件和MCU配置文件 |
| Hook   | CosyOS已经为用户创建好了四个系统钩子函数，用户直接写代码即可 |

### 文档中心
#### [技术要点](https://gitee.com/cosyos/cosyos/blob/master/%E6%8A%80%E6%9C%AF%E8%A6%81%E7%82%B9.md)
#### [开发流程](https://gitee.com/cosyos/cosyos/blob/master/%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B.md)
#### [API用户参考手册](https://gitee.com/cosyos/cosyos/blob/master/CosyOS-API%E7%94%A8%E6%88%B7%E5%8F%82%E8%80%83%E6%89%8B%E5%86%8C.docx)
#### [全局不关中断原理](https://gitee.com/cosyos/cosyos/blob/master/CosyOS-%E6%89%80%E6%9C%89%E5%86%85%E6%A0%B8%E5%85%A8%E5%B1%80%E4%B8%8D%E5%85%B3%E4%B8%AD%E6%96%AD%E5%8E%9F%E7%90%86.md)

### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

 _CosyOS现征集试用者，可免费获得作者的一对一在线指导，前提是您必须Watching或Star本仓库。您有什么疑问、意见或建议都可以提出来！_ <br>

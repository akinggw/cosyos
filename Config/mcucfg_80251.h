/**************************************************************************//**
 * @item     CosyOS Config
 * @file     mcucfg_80251.h
 * @brief    80251 Core Config File
 * @author   迟凯峰
 * @version  V2.3.4
 * @date     2023.05.09
 ******************************************************************************/

#ifndef __MCUCFG_80251_H
#define __MCUCFG_80251_H

/******************************************************************************
 *                             USER Definitions                               *
 ******************************************************************************/

          //*** <<< Use Configuration Wizard in Context Menu >>> ***//

///////////////////////////////////////////////////////////////////////////////
// <h> 编译器设置
// <i> 编译器设置

// <o> Code Rom Size
// <0=> Large <1=> Huge
// <i> 此项设置务必要与编译器的实际设置保持一致，否则CosyOS将无法正常运行。
#define SYSCFG_CODEROMSIZE              0

// <q> 4 Byte Interrupt Frame Size
// <i> 此项设置务必要与编译器的实际设置保持一致，否则CosyOS将无法正常运行。
#define SYSCFG_4BYTEINTFRAME            1

// </h>
///////////////////////////////////////////////////////////////////////////////
// <s> 标准头文件
// <i> 定义与您的MCU相匹配的标准头文件，您的c文件中可不必再包含此文件。
// <i> 示例一：STC32G.H
// <i> 示例二：..\MCU\STC32G.H，此种包含路径的写法，在图形用户界面输入时有时会出问题，建议在文本编辑界面中定义或加强校对。
#define MCUCFG_STANDARDHEAD             "STC32G.H"
///////////////////////////////////////////////////////////////////////////////
// <o> 系统时钟
// <i> 告知CosyOS您所配置的系统时钟，单位为MHZ。
#define MCUCFG_SYSFOSC                  24
#if !MCUCFG_SYSFOSC
#error 非法的设置值！
#endif
///////////////////////////////////////////////////////////////////////////////
// <o> 系统时钟分频系数
// <i> 告知CosyOS您所配置的系统时钟分频系数，取值范围：<1~255>。
#define MCUCFG_SYSFOSCDIV               1
#if !MCUCFG_SYSFOSCDIV
#error 非法的设置值！
#endif
///////////////////////////////////////////////////////////////////////////////
// <o> 栈内存
// <0=> idata <1=> edata
// <i> 如果您的MCU有edata内存，应配置栈内存为edata；否则，应配置栈内存为idata。
#define __MCUCFG_STACKMEM               1
#if __MCUCFG_STACKMEM == 0
#define _STACK_MEM_                     idata
#else
#define _STACK_MEM_                     edata
#endif
///////////////////////////////////////////////////////////////////////////////
// <h> 任务栈模式
// <i> 任务栈模式分为MSP模式、MSP+PSP模式（虚拟双栈指针模式），您只能选其一。
// <i> MSP模式，与51单片机的任务栈模式相同，任务切换时是要拷贝数据的，效率低，但同时却拥有100%可靠的任务栈重分配机制，只要主栈和内存池足够大就可确保所有任务栈永不溢出。
// <i> MSP+PSP模式，任务切换效率等同于Cortex-M，但每个任务栈都是主栈，需配置足够大的edata内存。
// <i> 如果您的MCU有edata内存且足够大，可以考虑MSP+PSP模式，以提高任务切换效率。

// <e> MSP模式
// <i> 是否启用MSP模式？
#define __MCUCFG_MSPMODE                0
// <o> 最低优先级中断寄存器库
// <0=> bank0 <1=> bank1 <2=> bank2 <3=> bank3
// <i> 所有最低优先级中断的寄存器库，bank0为不使用独立的寄存器库。
#define MCUCFG_STKREGBANK               0
// </e>

// <e> MSP+PSP模式
// <i> 是否启用MSP+PSP模式（虚拟双栈指针模式）？
#define __MCUCFG_MSPPSPMODE             1

#if   __MCUCFG_MSPMODE == 1 && __MCUCFG_MSPPSPMODE == 0
#define MCUCFG_TASKSTACKMODE            __MSP__
#elif __MCUCFG_MSPMODE == 0 && __MCUCFG_MSPPSPMODE == 1
#define MCUCFG_TASKSTACKMODE            __MSP_PSP__
#elif __MCUCFG_MSPMODE == 0 && __MCUCFG_MSPPSPMODE == 0
#error 任务栈模式未定义！
#elif __MCUCFG_MSPMODE == 1 && __MCUCFG_MSPPSPMODE == 1
#error 任务栈模式重定义！
#endif

#if MCUCFG_TASKSTACKMODE == __MSP_PSP__

// <o> 任务管理器的任务栈大小
// <i> min：45
#define __STACKSIZE_TASKMGR__           100
#if __STACKSIZE_TASKMGR__ < 45
#error 非法的设置值！
#endif

// <o> 系统调试任务的任务栈大小
// <i> min：42
#define __STACKSIZE_DEBUGGER__          100
#if __STACKSIZE_DEBUGGER__ < 42
#error 非法的设置值！
#endif

// <o> 系统启动任务的任务栈大小
// <i> min：42
#define __STACKSIZE_STARTER__           100
#if __STACKSIZE_STARTER__ < 42
#error 非法的设置值！
#endif

// <o> 系统空闲任务的任务栈大小
// <i> min：42
#define __STACKSIZE_SYSIDLE__           100
#if __STACKSIZE_SYSIDLE__ < 42
#error 非法的设置值！
#endif

// <q> 任务栈监控
// <i> 是否开启任务栈监控？
// <i> 开启后，CosyOS将自动插入任务栈监控代码至每一个任务和系统服务的开始处。
// <i> 而后，您应尽可能的把任务栈监控代码插入到每一个被任务调用的自定义函数内，并作为函数的第一句代码。
// <i> 然而，即便如此也无法实现100%可靠的任务栈监控，因为必然会有一部分函数无法插入任务栈监控代码，如标准库函数。
// <i> 只要插入了任务栈监控代码，便可准确的判断出，在此函数内是否存在任务栈溢出的风险。
// <i> 建议仅在调试阶段开启此项功能，正式的产品应禁用。
#define MCUCFG_TASKSTACKMONITOR         1

#endif
// </e>
// </h>
///////////////////////////////////////////////////////////////////////////////
// <h> PendSV_Handler
// <i> 由于251单片机没有PendSV_Handler软中断，您应专门配置一个硬件中断用于代替PendSV_Handler。
// <i> 您可选择一个未使用的硬件中断，并在初始化钩子中配置它，注意优先级应为最低级。

// <o> 中断号
// <i> 中断号
#define MCUCFG_PSVIRQ                   

// <o> 中断关闭
// <i> 此项您应在文本编辑界面中定义。
// <i> 示例：EX0 = 0
#define mPSV_Disable                    

// <o> 中断开启
// <i> 此项您应在文本编辑界面中定义。
// <i> 示例：EX0 = 1
#define mPSV_Enable                     

// <o> 中断触发
// <i> 置中断标志位
// <i> 此项您应在文本编辑界面中定义。
// <i> 示例：IE0 = 1
#define mPSV_Trigger                    

// <o> 中断清零
// <i> 清中断标志位
// <i> 此项您应在文本编辑界面中定义。
// <i> 示例：IE0 = 0
#define mPSV_Clear                      

// <o> 异步服务总数
// <i> 取值范围：<2~64>
// <i> 对于251单片机，您在中断中每调用一次异步服务，都要输入一个唯一的服务ID，此项用于定义您在中断中调用异步服务的总次数。
// <i> 示例：如果异步服务总数为10，那么您在中断中最多只能调用10次异步服务，服务ID为0~9。
#define MCUCFG_ISVTOTAL                 2
#if MCUCFG_ISVTOTAL < 2 || MCUCFG_ISVTOTAL > 64
#error 非法的设置值！
#endif

// </h>
///////////////////////////////////////////////////////////////////////////////
// <h> 内存优化
// <i> 内存优化是为了能让用户自由配置一部分系统变量的存储域，以达到最佳的内存利用率和尽可能的提高性能。
// <i> 每一项应尽可能的配置为更高性能的内存。

// <o> 定时中断内存
// <0=> data <1=> edata <2=> idata <3=> pdata <4=> xdata
// <i> 定时中断内存
#define __MCUCFG_MEMOPT1                1
#if   __MCUCFG_MEMOPT1 == 0
#define _TIMINT_MEM_                    data
#elif __MCUCFG_MEMOPT1 == 1
#define _TIMINT_MEM_                    edata
#elif __MCUCFG_MEMOPT1 == 2
#define _TIMINT_MEM_                    idata
#elif __MCUCFG_MEMOPT1 == 3
#define _TIMINT_MEM_                    pdata
#elif __MCUCFG_MEMOPT1 == 4
#define _TIMINT_MEM_                    xdata
#endif

// <o> 定时查询内存
// <0=> data <1=> edata <2=> idata <3=> pdata <4=> xdata
// <i> 定时查询内存
#define __MCUCFG_MEMOPT2                1
#if   __MCUCFG_MEMOPT2 == 0
#define _TIMQRY_MEM_                    data
#elif __MCUCFG_MEMOPT2 == 1
#define _TIMQRY_MEM_                    edata
#elif __MCUCFG_MEMOPT2 == 2
#define _TIMQRY_MEM_                    idata
#elif __MCUCFG_MEMOPT2 == 3
#define _TIMQRY_MEM_                    pdata
#elif __MCUCFG_MEMOPT2 == 4
#define _TIMQRY_MEM_                    xdata
#endif

// <o> DEBUG高速内存
// <0=> data <1=> edata <2=> idata <3=> pdata <4=> xdata
// <i> DEBUG高速内存
#define __MCUCFG_MEMOPT31               1
#if   __MCUCFG_MEMOPT31 == 0
#define _DEBUG_HMEM_                    data
#elif __MCUCFG_MEMOPT31 == 1
#define _DEBUG_HMEM_                    edata
#elif __MCUCFG_MEMOPT31 == 2
#define _DEBUG_HMEM_                    idata
#elif __MCUCFG_MEMOPT31 == 3
#define _DEBUG_HMEM_                    pdata
#elif __MCUCFG_MEMOPT31 == 4
#define _DEBUG_HMEM_                    xdata
#endif

// <o> DEBUG中速内存
// <0=> data <1=> edata <2=> idata <3=> pdata <4=> xdata
// <i> DEBUG中速内存
#define __MCUCFG_MEMOPT32               1
#if   __MCUCFG_MEMOPT32 == 0
#define _DEBUG_MMEM_                    data
#elif __MCUCFG_MEMOPT32 == 1
#define _DEBUG_MMEM_                    edata
#elif __MCUCFG_MEMOPT32 == 2
#define _DEBUG_MMEM_                    idata
#elif __MCUCFG_MEMOPT32 == 3
#define _DEBUG_MMEM_                    pdata
#elif __MCUCFG_MEMOPT32 == 4
#define _DEBUG_MMEM_                    xdata
#endif

// <o> DEBUG大内存
// <0=> edata <1=> xdata
// <i> DEBUG大内存
#define __MCUCFG_MEMOPT33               1
#if   __MCUCFG_MEMOPT33 == 0
#define _DEBUG_LMEM_                    edata
#elif __MCUCFG_MEMOPT33 == 1
#define _DEBUG_LMEM_                    xdata
#endif

// <o> 任务静态内存
// <0=> edata <1=> xdata
// <i> 任务静态内存
#define __MCUCFG_MEMOPT4                0
#if   __MCUCFG_MEMOPT4 == 0
#define _TASK_MEM_                      edata
#elif __MCUCFG_MEMOPT4 == 1
#define _TASK_MEM_                      xdata
#endif

// </h>
///////////////////////////////////////////////////////////////////////////////
// <h> 动态内存设置
// <i> CosyOS会使用下列参数自动初始化内存池。

// <o> 内存池指针
// <i> 内存池的起始内存地址
#define MCUCFG_MALLOCMEMBPTR            4096
#if !MCUCFG_MALLOCMEMBPTR
#error 非法的设置值！
#endif

// <o> 内存池大小
// <i> 内存池的大小（字节数）
#define MCUCFG_MALLOCMEMSIZE            4096
#if !MCUCFG_MALLOCMEMSIZE
#error 非法的设置值！
#endif

// </h>
///////////////////////////////////////////////////////////////////////////////

                //*** <<< end of configuration section >>> ***//

/******************************************************************************
 *                               OS Definitions                               *
 ******************************************************************************/

/* Header */
#include <string.h>
#include <intrins.h>
#include MCUCFG_STANDARDHEAD
#include "..\System\vardef.h"

/* Memory */
#define _CODE_MEM_    code
#define _SYS_MEM_     data
#define _SV_MEM_
#define _CONST_MEM_
#ifndef _MALLOC_MEM_
#define _MALLOC_MEM_  far
#endif
#if SYSCFG_TASKCREATEMODE == __STATIC__ || SYSCFG_TASKCREATEMODE == __BALANCE__
#define _THDL_MEM_    _MALLOC_MEM_
#elif SYSCFG_TASKCREATEMODE == __DYNAMIC__
#define _THDL_MEM_    _TASK_MEM_
#endif

/* Register */
#define _SYS_REG_
#define _DEBUG_HREG_
#define _DEBUG_LREG_

/* Typedef */
typedef bit   tBIT;
typedef u16   tSP;
typedef u16   tStackSize;
#if SYSCFG_CODEROMSIZE == 0
typedef u16   tPSP;
#else
typedef u32   tPSP;
#endif
typedef u8    tDM;
typedef u32   tPC;
typedef u32   tGRP;
typedef u32   tSysTick;

/* Extern */
extern bit vISV_FF;
extern u32 _SYS_MEM_ vPSP;
extern tSP _SYS_MEM_ vMSP;
extern tSP _SYS_MEM_ vBSP;
extern void _SV_MEM_ * _TASK_MEM_ vISS[MCUCFG_ISVTOTAL];
extern void __entry_monitor(void);

/* * */
#define MCUCFG_MCUARC         !__ARM__
#define MCUCFG_MCULEVEL       __ZHENGJINGPAOZI__
#define __C51USING__
#define __STK_ATTRIBUTE__     interrupt 1 __USING__
#define __PSV_ATTRIBUTE__     interrupt MCUCFG_PSVIRQ __USING__
#define __REENTRANT__         reentrant
#define __BSP_ALIGN__
#define MCUCFG_DIRMSGTYPE     0
#define __DM_PSP__
#define __DM_SIZE__           (&mx - &m0 - 1)
#if SYSCFG_DIRMSGMODE == __PERFORMANCE__
#define __DM_VAR__            tDM m0_
#define __DM_VAL__            1
#elif SYSCFG_DIRMSGMODE == __INTELLIGENT__
#define __DM_VAR__            u32 DR0_, u32 DR4_, tDM R11_, tDM m0_
#define __DM_VAL__            0, 0, 0, 1
#endif

/* API */
#define mSysTick_InitValue    (65536 - (1UL * MCUCFG_SYSFOSC * SYSCFG_STKCYCLE) / MCUCFG_SYSFOSCDIV / 12)
#define mSysTick_Cycle        (65536 - mSysTick_InitValue)
#define mSysTick_Counter      ((TH0 << 8) | TL0)
#define mSTK_Disable          ET0 = 0
#define mSTK_Enable           ET0 = 1

#define mDisableIRQ	EA = 0
#define mEnableIRQ	EA = 1

#define mEnterCritical	\
do{	\
	mPSV_Disable;	\
	mSTK_Disable;	\
}while(false)

#define mExitCritical	\
do{	\
	mSTK_Enable;	\
	mPSV_Enable;	\
}while(false)

#define mSys_Idle	\
do{	\
	PCON |= 0x01;	\
	OS_NOPxX;	\
}while(false)

#define mSys_init	\
do{	\
	__init_mempool((void _MALLOC_MEM_ *)MCUCFG_MALLOCMEMBPTR, MCUCFG_MALLOCMEMSIZE);	\
	OS_NOPx1;	\
	TMOD &= 0xF0;	\
	TL0 = (u8)(mSysTick_InitValue);	\
	TH0 = (u8)(mSysTick_InitValue >> 8);	\
	TR0 = 1;	\
	mSTK_Enable;	\
	mPSV_Enable;	\
	EA = 1;	\
}while(false)

#define mTaskmgr_Counting	\
	counter2 = (counter1 * 100 * 12 * MCUCFG_SYSFOSCDIV) / MCUCFG_SYSFOSC / counter2

#define mSTK_Counting	\
do{	\
	if(mSysTick_Counter <= tick_temp) break;	\
	vSTK_Counter1 += mSysTick_Counter - tick_temp;	\
	vSTK_Counter2++;	\
}while(false)

#if SYSCFG_TASKPCMONITOR == __ENABLED__
#define mTaskPC_Monitor	\
do{	\
	if(!vTaskmgrBinary) break;	\
	vPSP -= (__BASICSTACKSIZE__ - 1);	\
	vPC = *(tPC *)vPSP;	\
	vPC = ((u16)vPC << 8) | ((u16)vPC >> 8) | (vPC & 0xFFFF0000);	\
}while(false)
#else
#define mTaskPC_Monitor do{}while(false)
#endif

#define mUsedTime_END	\
do{	\
	if(usedtime[0])	\
	{	\
		usedtime[0]--;	\
		usedtime[1] = 65536 - usedtime[1] + counter - mSysTick_InitValue;	\
	}	\
	else	\
	{	\
		if(counter >= usedtime[1])	\
		{	\
			usedtime[1] = counter - usedtime[1];	\
		}	\
		else	\
		{	\
			usedtime[1] = 65536 - usedtime[1] + counter - mSysTick_InitValue;	\
		}	\
	}	\
	vTASKING->usedtime[0] += usedtime[0];	\
	vTASKING->usedtime[0] += (vTASKING->usedtime[1] + usedtime[1]) / mSysTick_Cycle;	\
	vTASKING->usedtime[1]  = (vTASKING->usedtime[1] + usedtime[1]) % mSysTick_Cycle;	\
}while(false)

#define mUsedTime_INIT	\
do{	\
	usedtime[0] = 0;	\
	usedtime[1] = counter;	\
}while(false)

#define mISV_Do(svid)	\
do{	\
	extern bit vISV_F##svid;	\
	vISS[svid] = &isv_;	\
	vISV_F##svid = true;	\
	vISV_FF = true;	\
	mPSV_Trigger;	\
}while(false)

#define mISV	\
	extern void __isv_handler(void);	\
	mPSV_Clear;	\
	if(vISV_FF) __isv_handler();	\
	if(!vScheduling_f) return

#define miWriteFlagBits	\
	static bit flag = false;	\
	if(!flag)	\
	{	\
		flag = true



/*
 * MSP模式
 */

#if MCUCFG_TASKSTACKMODE == __MSP__

/* * */
#define MCUCFG_TASKSTACKMONITOR __DISABLED__
#if MCUCFG_STKREGBANK
#define __USING__               using MCUCFG_STKREGBANK
#define __REGSIZE__             8
#define __BASICSTACKSIZE__      (32 - SYSCFG_4BYTEINTFRAME)
#else
#define __USING__
#define __REGSIZE__             0
#define __BASICSTACKSIZE__      (40 - SYSCFG_4BYTEINTFRAME)
#endif
#define __STACKSIZE_TASKMGR__   (__BASICSTACKSIZE__ + 16)
#define __STACKSIZE_DEBUGGER__  (__BASICSTACKSIZE__ + 16)
#define __STACKSIZE_STARTER__   (__BASICSTACKSIZE__ + 16)
#define __STACKSIZE_SYSIDLE__   (__BASICSTACKSIZE__ + 16)

/* API */
#define mTaskNode_Head_       tStackSize stacklen;
#if __REGSIZE__
#define mTaskNode_Tail_       u8 reg[__REGSIZE__];
#define mReg_PUSH	\
	*(u32 *)(vTASKING->reg + 0) = *(u32 _SYS_MEM_ *)0;	\
	*(u32 *)(vTASKING->reg + 4) = *(u32 _SYS_MEM_ *)4
#define mReg_POP	\
	*(u32 _SYS_MEM_ *)0 = *(u32 *)(vTASKING->reg + 0);	\
	*(u32 _SYS_MEM_ *)4 = *(u32 *)(vTASKING->reg + 4)
#else
#define mTaskNode_Tail_
#define mReg_PUSH do{}while(false)
#define mReg_POP  do{}while(false)
#endif

#define mSys_INIT	\
do{	\
	vMSP = (SPH << 8) | SP;	\
	vBSP = vMSP + 1;	\
	mSys_init;	\
}while(false)

#define mScheduler_INIT	\
	u8  i;	\
	u8  _STACK_MEM_  * _SYS_REG_ msp8;	\
	u8  _MALLOC_MEM_ * _SYS_REG_ psp8;	\
	u32 _STACK_MEM_  * _SYS_REG_ msp32;	\
	u32 _MALLOC_MEM_ * _SYS_REG_ psp32;	\
	bit vPUSH_f = false;	\
	vScheduling_f = false;	\
	__asm{MOV vPSP, DR60}

#define mTaskStack_INIT	\
do{	\
	*(u32 *)node_news->BSP = ((u16)vACTBUF->entry << 8) | ((u16)vACTBUF->entry >> 8) | ((u32)vACTBUF->entry & 0xFFFF0000);	\
	*(u8 *)(node_news->BSP + __BASICSTACKSIZE__ - 3) = 0;	\
	node_news->stacklen = __BASICSTACKSIZE__;	\
}while(false)

#define mTaskStack_LEN	\
	stacklen = (tSP)vPSP - vMSP

#define mEvery_Monitor do{}while(false)
#define mEntry_Monitor do{}while(false)

#if SYSCFG_TASKCREATEMODE == __STATIC__
#define mPUSH_Monitor	\
do{	\
	if(vTASKING->stacksize < stacklen)	\
	{	\
		vTASKING->state = __STOPPED_TSOF__;	\
		vFault.overflow_task_stack = true;	\
	}	\
	else	\
	{	\
		vPUSH_f = true;	\
		if(vTASKING->TPL > node_news->TPL)	\
		{	\
			vTASKING->counter = 0;	\
		}	\
	}	\
}while(false)
#else
#define mPUSH_Monitor	\
do{	\
	if(vTASKING->stacksize < stacklen)	\
	{	\
		vTASKING->stacksize = stacklen;	\
		__free(vTASKING->BSP);	\
		vTASKING->BSP = NULL;	\
		vTASKING->BSP = (u8 *)__malloc(vTASKING->stacksize);	\
		if(vTASKING->BSP != NULL)	\
		{	\
			vTASKING->realloc = true;	\
			vAlarm.realloc_task_stack = true;	\
			vPUSH_f = true;	\
		}	\
		else	\
		{	\
			vTASKING->state = __STOPPED_TSRF__;	\
			vFault.realloc_fail_task_stack = true;	\
		}	\
	}	\
	else	\
	{	\
		vPUSH_f = true;	\
	}	\
	if(vPUSH_f)	\
	{	\
		if(vTASKING->TPL > node_news->TPL)	\
		{	\
			vTASKING->counter = 0;	\
		}	\
	}	\
}while(false)
#endif

#define mTaskStack_PUSH	\
do{	\
	if(!vPUSH_f) break;	\
	if(vTaskmgrBinary) mUsedTime_END;	\
	msp32 = (u32 _STACK_MEM_  *)vBSP;	\
	psp32 = (u32 _MALLOC_MEM_ *)vTASKING->BSP;	\
	vTASKING->stacklen = stacklen;	\
	i = stacklen >> 2;	\
	do{	\
		*psp32++ = *msp32++;	\
	}while(--i);	\
	i = stacklen & 3;	\
	if(i)	\
	{	\
		msp8 = (u8 _STACK_MEM_  *)msp32;	\
		psp8 = (u8 _MALLOC_MEM_ *)psp32;	\
		do{	\
			*psp8++ = *msp8++;	\
		}while(--i);	\
	}	\
	mReg_PUSH;	\
	mTaskPC_Monitor;	\
}while(false)

#define mTaskStack_POP	\
do{	\
	if(vTaskmgrBinary) mUsedTime_INIT;	\
	vTASKING = node_news;	\
	msp32 = (u32 _STACK_MEM_  *)vBSP;	\
	psp32 = (u32 _MALLOC_MEM_ *)vTASKING->BSP;	\
	stacklen = vTASKING->stacklen;	\
	i = stacklen >> 2;	\
	do{	\
		*msp32++ = *psp32++;	\
	}while(--i);	\
	i = stacklen & 3;	\
	if(i)	\
	{	\
		msp8 = (u8 _STACK_MEM_  *)msp32;	\
		psp8 = (u8 _MALLOC_MEM_ *)psp32;	\
		do{	\
			*msp8++ = *psp8++;	\
		}while(--i);	\
	}	\
	mReg_POP;	\
	vPSP = vMSP + stacklen;	\
	__asm{MOV DR60, vPSP};	\
}while(false)



/*
 * MSP+PSP模式
 */

#elif MCUCFG_TASKSTACKMODE == __MSP_PSP__

/* * */
#define __USING__
#define __BASICSTACKSIZE__    (40 - SYSCFG_4BYTEINTFRAME)

/* API */
#define mTaskNode_Head_       tPSP PSP;
#define mTaskNode_Tail_

#define mSys_INIT	\
do{	\
	vMSP = (SPH << 8) | SP;	\
	mSys_init;	\
}while(false)

#define mScheduler_INIT	\
	bit vPUSH_f = false;	\
	vScheduling_f = false;	\
	__asm{MOV vPSP, DR60}

#define mTaskStack_INIT	\
do{	\
	*(u32 *)node_news->BSP = ((u16)vACTBUF->entry << 8) | ((u16)vACTBUF->entry >> 8) | ((u32)vACTBUF->entry & 0xFFFF0000);	\
	*(u8 *)(node_news->BSP + __BASICSTACKSIZE__ - 3) = 0;	\
	node_news->PSP = (tSP)node_news->BSP + __BASICSTACKSIZE__ - 1;	\
}while(false)

#define mTaskStack_LEN	\
	stacklen = (tSP)vPSP + 1 - (tSP)vTASKING->BSP

#if MCUCFG_TASKSTACKMONITOR == __ENABLED__
#define mEvery_Monitor	\
do{	\
	if(vTASKING->state == __STOPPED_TSOF__)	\
	{	\
		vFault.overflow_task_stack = true;	\
	}	\
	else if(vTASKING->stacksize < stacklen)	\
	{	\
		vTASKING->state = __STOPPED_TSOF__;	\
		vFault.overflow_task_stack = true;	\
	}	\
}while(false)
#define mEntry_Monitor __entry_monitor()
#else
#define mEvery_Monitor do{}while(false)
#define mEntry_Monitor do{}while(false)
#endif

#define mPUSH_Monitor	\
do{	\
	vPUSH_f = true;	\
	if(vTASKING->TPL > node_news->TPL)	\
	{	\
		vTASKING->counter = 0;	\
	}	\
}while(false)

#define mTaskStack_PUSH	\
do{	\
	if(!vPUSH_f) break;	\
	if(vTaskmgrBinary) mUsedTime_END;	\
	vTASKING->PSP = vPSP;	\
	mTaskPC_Monitor;	\
}while(false)

#define mTaskStack_POP	\
do{	\
	if(vTaskmgrBinary) mUsedTime_INIT;	\
	vTASKING = node_news;	\
	vPSP = vTASKING->PSP;	\
	__asm{MOV DR60, vPSP};	\
}while(false)

#endif

#endif

/**************************************************************************//**
 * @item     CosyOS Config
 * @file     mcucfg_8051.c
 * @brief    8051 Core Config File
 * @author   迟凯峰
 * @version  V2.3.0
 * @date     2023.04.25
 ******************************************************************************/

#include "..\System\glovar.h"
#ifdef __MCUCFG_8051_H

#include "..\System\sysapi.h"
#include "..\System\svs.h"
tSP _SYS_MEM_ vMSP;
tSP _SYS_MEM_ vBSP;

#if MCUCFG_ISVTOTAL > 0
u16 _TASK_MEM_ vISS[MCUCFG_ISVTOTAL];
bit vISV_FF = false;
bit vISV_F0 = false;
#if MCUCFG_ISVTOTAL > 1
bit vISV_F1 = false;
#if MCUCFG_ISVTOTAL > 2
bit vISV_F2 = false;
#if MCUCFG_ISVTOTAL > 3
bit vISV_F3 = false;
#if MCUCFG_ISVTOTAL > 4
bit vISV_F4 = false;
#if MCUCFG_ISVTOTAL > 5
bit vISV_F5 = false;
#if MCUCFG_ISVTOTAL > 6
bit vISV_F6 = false;
#if MCUCFG_ISVTOTAL > 7
bit vISV_F7 = false;
#if MCUCFG_ISVTOTAL > 8
bit vISV_F8 = false;
#if MCUCFG_ISVTOTAL > 9
bit vISV_F9 = false;
#if MCUCFG_ISVTOTAL > 10
bit vISV_F10 = false;
#if MCUCFG_ISVTOTAL > 11
bit vISV_F11 = false;
#if MCUCFG_ISVTOTAL > 12
bit vISV_F12 = false;
#if MCUCFG_ISVTOTAL > 13
bit vISV_F13 = false;
#if MCUCFG_ISVTOTAL > 14
bit vISV_F14 = false;
#if MCUCFG_ISVTOTAL > 15
bit vISV_F15 = false;
#if MCUCFG_ISVTOTAL > 16
bit vISV_F16 = false;
#if MCUCFG_ISVTOTAL > 17
bit vISV_F17 = false;
#if MCUCFG_ISVTOTAL > 18
bit vISV_F18 = false;
#if MCUCFG_ISVTOTAL > 19
bit vISV_F19 = false;
#if MCUCFG_ISVTOTAL > 20
bit vISV_F20 = false;
#if MCUCFG_ISVTOTAL > 21
bit vISV_F21 = false;
#if MCUCFG_ISVTOTAL > 22
bit vISV_F22 = false;
#if MCUCFG_ISVTOTAL > 23
bit vISV_F23 = false;
#if MCUCFG_ISVTOTAL > 24
bit vISV_F24 = false;
#if MCUCFG_ISVTOTAL > 25
bit vISV_F25 = false;
#if MCUCFG_ISVTOTAL > 26
bit vISV_F26 = false;
#if MCUCFG_ISVTOTAL > 27
bit vISV_F27 = false;
#if MCUCFG_ISVTOTAL > 28
bit vISV_F28 = false;
#if MCUCFG_ISVTOTAL > 29
bit vISV_F29 = false;
#if MCUCFG_ISVTOTAL > 30
bit vISV_F30 = false;
#if MCUCFG_ISVTOTAL > 31
bit vISV_F31 = false;
#if MCUCFG_ISVTOTAL > 32
bit vISV_F32 = false;
#if MCUCFG_ISVTOTAL > 33
bit vISV_F33 = false;
#if MCUCFG_ISVTOTAL > 34
bit vISV_F34 = false;
#if MCUCFG_ISVTOTAL > 35
bit vISV_F35 = false;
#if MCUCFG_ISVTOTAL > 36
bit vISV_F36 = false;
#if MCUCFG_ISVTOTAL > 37
bit vISV_F37 = false;
#if MCUCFG_ISVTOTAL > 38
bit vISV_F38 = false;
#if MCUCFG_ISVTOTAL > 39
bit vISV_F39 = false;
#if MCUCFG_ISVTOTAL > 40
bit vISV_F40 = false;
#if MCUCFG_ISVTOTAL > 41
bit vISV_F41 = false;
#if MCUCFG_ISVTOTAL > 42
bit vISV_F42 = false;
#if MCUCFG_ISVTOTAL > 43
bit vISV_F43 = false;
#if MCUCFG_ISVTOTAL > 44
bit vISV_F44 = false;
#if MCUCFG_ISVTOTAL > 45
bit vISV_F45 = false;
#if MCUCFG_ISVTOTAL > 46
bit vISV_F46 = false;
#if MCUCFG_ISVTOTAL > 47
bit vISV_F47 = false;
#if MCUCFG_ISVTOTAL > 48
bit vISV_F48 = false;
#if MCUCFG_ISVTOTAL > 49
bit vISV_F49 = false;
#if MCUCFG_ISVTOTAL > 50
bit vISV_F50 = false;
#if MCUCFG_ISVTOTAL > 51
bit vISV_F51 = false;
#if MCUCFG_ISVTOTAL > 52
bit vISV_F52 = false;
#if MCUCFG_ISVTOTAL > 53
bit vISV_F53 = false;
#if MCUCFG_ISVTOTAL > 54
bit vISV_F54 = false;
#if MCUCFG_ISVTOTAL > 55
bit vISV_F55 = false;
#if MCUCFG_ISVTOTAL > 56
bit vISV_F56 = false;
#if MCUCFG_ISVTOTAL > 57
bit vISV_F57 = false;
#if MCUCFG_ISVTOTAL > 58
bit vISV_F58 = false;
#if MCUCFG_ISVTOTAL > 59
bit vISV_F59 = false;
#if MCUCFG_ISVTOTAL > 60
bit vISV_F60 = false;
#if MCUCFG_ISVTOTAL > 61
bit vISV_F61 = false;
#if MCUCFG_ISVTOTAL > 62
bit vISV_F62 = false;
#if MCUCFG_ISVTOTAL > 63
bit vISV_F63 = false;
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif

static void __isv(u8 svid) __C51USING__
{
	register void _SV_MEM_ *svs = (void _SV_MEM_ *)(vISS[svid]);
	sISS_Handler
}

void __isv_handler(void) __USING__
{
	while(vISV_FF)
	{
		vISV_FF = false;
		#if MCUCFG_ISVTOTAL > 0
		if(vISV_F0)
		{
			vISV_F0 = false;
			__isv(0);
		}
		#if MCUCFG_ISVTOTAL > 1
		if(vISV_F1)
		{
			vISV_F1 = false;
			__isv(1);
		}
		#if MCUCFG_ISVTOTAL > 2
		if(vISV_F2)
		{
			vISV_F2 = false;
			__isv(2);
		}
		#if MCUCFG_ISVTOTAL > 3
		if(vISV_F3)
		{
			vISV_F3 = false;
			__isv(3);
		}
		#if MCUCFG_ISVTOTAL > 4
		if(vISV_F4)
		{
			vISV_F4 = false;
			__isv(4);
		}
		#if MCUCFG_ISVTOTAL > 5
		if(vISV_F5)
		{
			vISV_F5 = false;
			__isv(5);
		}
		#if MCUCFG_ISVTOTAL > 6
		if(vISV_F6)
		{
			vISV_F6 = false;
			__isv(6);
		}
		#if MCUCFG_ISVTOTAL > 7
		if(vISV_F7)
		{
			vISV_F7 = false;
			__isv(7);
		}
		#if MCUCFG_ISVTOTAL > 8
		if(vISV_F8)
		{
			vISV_F8 = false;
			__isv(8);
		}
		#if MCUCFG_ISVTOTAL > 9
		if(vISV_F9)
		{
			vISV_F9 = false;
			__isv(9);
		}
		#if MCUCFG_ISVTOTAL > 10
		if(vISV_F10)
		{
			vISV_F10 = false;
			__isv(10);
		}
		#if MCUCFG_ISVTOTAL > 11
		if(vISV_F11)
		{
			vISV_F11 = false;
			__isv(11);
		}
		#if MCUCFG_ISVTOTAL > 12
		if(vISV_F12)
		{
			vISV_F12 = false;
			__isv(12);
		}
		#if MCUCFG_ISVTOTAL > 13
		if(vISV_F13)
		{
			vISV_F13 = false;
			__isv(13);
		}
		#if MCUCFG_ISVTOTAL > 14
		if(vISV_F14)
		{
			vISV_F14 = false;
			__isv(14);
		}
		#if MCUCFG_ISVTOTAL > 15
		if(vISV_F15)
		{
			vISV_F15 = false;
			__isv(15);
		}
		#if MCUCFG_ISVTOTAL > 16
		if(vISV_F16)
		{
			vISV_F16 = false;
			__isv(16);
		}
		#if MCUCFG_ISVTOTAL > 17
		if(vISV_F17)
		{
			vISV_F17 = false;
			__isv(17);
		}
		#if MCUCFG_ISVTOTAL > 18
		if(vISV_F18)
		{
			vISV_F18 = false;
			__isv(18);
		}
		#if MCUCFG_ISVTOTAL > 19
		if(vISV_F19)
		{
			vISV_F19 = false;
			__isv(19);
		}
		#if MCUCFG_ISVTOTAL > 20
		if(vISV_F20)
		{
			vISV_F20 = false;
			__isv(20);
		}
		#if MCUCFG_ISVTOTAL > 21
		if(vISV_F21)
		{
			vISV_F21 = false;
			__isv(21);
		}
		#if MCUCFG_ISVTOTAL > 22
		if(vISV_F22)
		{
			vISV_F22 = false;
			__isv(22);
		}
		#if MCUCFG_ISVTOTAL > 23
		if(vISV_F23)
		{
			vISV_F23 = false;
			__isv(23);
		}
		#if MCUCFG_ISVTOTAL > 24
		if(vISV_F24)
		{
			vISV_F24 = false;
			__isv(24);
		}
		#if MCUCFG_ISVTOTAL > 25
		if(vISV_F25)
		{
			vISV_F25 = false;
			__isv(25);
		}
		#if MCUCFG_ISVTOTAL > 26
		if(vISV_F26)
		{
			vISV_F26 = false;
			__isv(26);
		}
		#if MCUCFG_ISVTOTAL > 27
		if(vISV_F27)
		{
			vISV_F27 = false;
			__isv(27);
		}
		#if MCUCFG_ISVTOTAL > 28
		if(vISV_F28)
		{
			vISV_F28 = false;
			__isv(28);
		}
		#if MCUCFG_ISVTOTAL > 29
		if(vISV_F29)
		{
			vISV_F29 = false;
			__isv(29);
		}
		#if MCUCFG_ISVTOTAL > 30
		if(vISV_F30)
		{
			vISV_F30 = false;
			__isv(30);
		}
		#if MCUCFG_ISVTOTAL > 31
		if(vISV_F31)
		{
			vISV_F31 = false;
			__isv(31);
		}
		#if MCUCFG_ISVTOTAL > 32
		if(vISV_F32)
		{
			vISV_F32 = false;
			__isv(32);
		}
		#if MCUCFG_ISVTOTAL > 33
		if(vISV_F33)
		{
			vISV_F33 = false;
			__isv(33);
		}
		#if MCUCFG_ISVTOTAL > 34
		if(vISV_F34)
		{
			vISV_F34 = false;
			__isv(34);
		}
		#if MCUCFG_ISVTOTAL > 35
		if(vISV_F35)
		{
			vISV_F35 = false;
			__isv(35);
		}
		#if MCUCFG_ISVTOTAL > 36
		if(vISV_F36)
		{
			vISV_F36 = false;
			__isv(36);
		}
		#if MCUCFG_ISVTOTAL > 37
		if(vISV_F37)
		{
			vISV_F37 = false;
			__isv(37);
		}
		#if MCUCFG_ISVTOTAL > 38
		if(vISV_F38)
		{
			vISV_F38 = false;
			__isv(38);
		}
		#if MCUCFG_ISVTOTAL > 39
		if(vISV_F39)
		{
			vISV_F39 = false;
			__isv(39);
		}
		#if MCUCFG_ISVTOTAL > 40
		if(vISV_F40)
		{
			vISV_F40 = false;
			__isv(40);
		}
		#if MCUCFG_ISVTOTAL > 41
		if(vISV_F41)
		{
			vISV_F41 = false;
			__isv(41);
		}
		#if MCUCFG_ISVTOTAL > 42
		if(vISV_F42)
		{
			vISV_F42 = false;
			__isv(42);
		}
		#if MCUCFG_ISVTOTAL > 43
		if(vISV_F43)
		{
			vISV_F43 = false;
			__isv(43);
		}
		#if MCUCFG_ISVTOTAL > 44
		if(vISV_F44)
		{
			vISV_F44 = false;
			__isv(44);
		}
		#if MCUCFG_ISVTOTAL > 45
		if(vISV_F45)
		{
			vISV_F45 = false;
			__isv(45);
		}
		#if MCUCFG_ISVTOTAL > 46
		if(vISV_F46)
		{
			vISV_F46 = false;
			__isv(46);
		}
		#if MCUCFG_ISVTOTAL > 47
		if(vISV_F47)
		{
			vISV_F47 = false;
			__isv(47);
		}
		#if MCUCFG_ISVTOTAL > 48
		if(vISV_F48)
		{
			vISV_F48 = false;
			__isv(48);
		}
		#if MCUCFG_ISVTOTAL > 49
		if(vISV_F49)
		{
			vISV_F49 = false;
			__isv(49);
		}
		#if MCUCFG_ISVTOTAL > 50
		if(vISV_F50)
		{
			vISV_F50 = false;
			__isv(50);
		}
		#if MCUCFG_ISVTOTAL > 51
		if(vISV_F51)
		{
			vISV_F51 = false;
			__isv(51);
		}
		#if MCUCFG_ISVTOTAL > 52
		if(vISV_F52)
		{
			vISV_F52 = false;
			__isv(52);
		}
		#if MCUCFG_ISVTOTAL > 53
		if(vISV_F53)
		{
			vISV_F53 = false;
			__isv(53);
		}
		#if MCUCFG_ISVTOTAL > 54
		if(vISV_F54)
		{
			vISV_F54 = false;
			__isv(54);
		}
		#if MCUCFG_ISVTOTAL > 55
		if(vISV_F55)
		{
			vISV_F55 = false;
			__isv(55);
		}
		#if MCUCFG_ISVTOTAL > 56
		if(vISV_F56)
		{
			vISV_F56 = false;
			__isv(56);
		}
		#if MCUCFG_ISVTOTAL > 57
		if(vISV_F57)
		{
			vISV_F57 = false;
			__isv(57);
		}
		#if MCUCFG_ISVTOTAL > 58
		if(vISV_F58)
		{
			vISV_F58 = false;
			__isv(58);
		}
		#if MCUCFG_ISVTOTAL > 59
		if(vISV_F59)
		{
			vISV_F59 = false;
			__isv(59);
		}
		#if MCUCFG_ISVTOTAL > 60
		if(vISV_F60)
		{
			vISV_F60 = false;
			__isv(60);
		}
		#if MCUCFG_ISVTOTAL > 61
		if(vISV_F61)
		{
			vISV_F61 = false;
			__isv(61);
		}
		#if MCUCFG_ISVTOTAL > 62
		if(vISV_F62)
		{
			vISV_F62 = false;
			__isv(62);
		}
		#if MCUCFG_ISVTOTAL > 63
		if(vISV_F63)
		{
			vISV_F63 = false;
			__isv(63);
		}
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
		#endif
	}
}

#endif

#endif

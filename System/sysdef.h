/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     sysdef.h
 * @brief    系统定义
 * @author   迟凯峰
 * @version  V2.3.0
 * @date     2023.04.25
 ******************************************************************************/

#ifndef __SYSDEF_H
#define __SYSDEF_H

/*------------------------------------------------------------------------------
 * SYS-API */

#define	sDefBitField(v)     volatile u8 v:1
#define	sDefVoidBits(n)     volatile u8  :n
#define	sDefStr(s)          #s
#define	sReDefStr(s)        sDefStr(s)
#define	sCatStr(s1, s2)     s1##s2
#define	sReCatStr(s1, s2)   sCatStr(s1, s2)

/*------------------------------------------------------------------------------
 * CONST */

#define false   0
#define true    1

/*
 * Task
 */

#define __TASKQUEUEMAXLEN__ 255

/* The Task State */
#define	__READY__           0x00
#define	__FLOATING__        0x01
#define	__BLOCKED__         0x02
#define __OVERTIME__        0x04
#define	__SUSPENDED__       0x08
#define	__STOPPED__         0x10
#define	__STOPPED_TSOF__    0x20	// stopped on task stack overflow
#define __STOPPED_TSRF__    0x40	// stopped on task stack realloc is failed
#define	__DELETED__         0xFF

/* The Operation */
#define	__SUSPEND__         __SUSPENDED__
#define	__RESUME__          0xF7
#define	__DELETE__          __DELETED__
#define __RESSUS__          0x80

/* Block Type */
#define	__BINARY_WAIT__     0x0001
#define	__BINARY_TAKE__     0x0002
#define	__BINARY__          0x0003
#define	__MUTEX__           0x0004
#define	__SEMAPHORE__       0x0008
#define __FLAGGROUP_1__     0x0010
#define __FLAGGROUP_2__     0x0020
#define __FLAGGROUP_4__     0x0040
#define	__FLAGGROUP__       0x0070
#define	__RECV_DM__         0x0100
#define	__RECV_MAIL__       0x0200
#define	__RECV_MSG__        0x0400
#define	__DELAY__           0x8000



/*
 * Message Type
 */

#define	__CONSTR__          0
#define	__CONARY__          1
#define	__VARSTR__          2
#define	__VARARY__          3



/*
 * Setup
 */

#define	__NO__              0
#define	__YES__             1
#define __DISABLED__        0
#define __ENABLED__         1
#define __STATIC__          0
#define __BALANCE__         1
#define __PUBLIC__          1
#define __DYNAMIC__         2
#define __MSP__             0
#define __MSP_PSP__         1
#define	__STCLK__           0
#define	__FCLK__            1
#define	__PERFORMANCE__     0
#define __INTELLIGENT__     1
#define __C89__             0
#define __C99__             1
#define __ARM__             1
#define __FEIPAOZI__        0
#define __ZHENGJINGPAOZI__  1
#define __MSGQUE_FIFO__     0
#define __MSGQUE_LIFO__     1



/*
 * Taskmgr
 */

/* up speed: unit is ms */
#define	__FAST__            500
#define	__MIDDLE__          1000
#define	__SLOW__            2000

/* LEN */
#define	__TPLLEN__          6
#define __STALEN__          6
#define	__CPULEN__          9



/*
 * svid
 */

#define __SVID_RESTASK__    0x00
#define __SVID_SUSTASK__    0x01
#define __SVID_DELTASK__    0x02
#define __SVID_SETTPL__     0x03
#define __SVID_TIMINT__     0x04
#define __SVID_TIMQRY__     0x05
#define __SVID_FREESEM__    0x06
#define __SVID_WRITEVAR__   0x07
#define __SVID_SELFOPE__    0x08
#define __SVID_SENDMAIL__   0x09
#define __SVID_SENDMSG__    0x0A
#define __SVID_WRITEGRP__   0x0B



#endif

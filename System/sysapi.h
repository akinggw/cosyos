/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     sysapi.h
 * @brief    系统API
 * @author   迟凯峰
 * @version  V2.3.1
 * @date     2023.05.02
 ******************************************************************************/

#ifndef __SYSAPI_H
#define __SYSAPI_H

#define __init_mempool  init_mempool
#define __malloc  malloc
#define __calloc  calloc
#define __realloc realloc
#define __free    free
#define __memcpy  memcpy
#define __strcpy  strcpy
#define __strlen  strlen
#define __strcmp  strcmp



/*
 * 任务
 */

#if SYSCFG_TASKCREATEMODE == __STATIC__ || SYSCFG_TASKCREATEMODE == __BALANCE__
#define sTaskNode(task_hand) (task_hand)
#elif SYSCFG_TASKCREATEMODE == __DYNAMIC__
#define sTaskNode(task_hand) (task_hand)->task_node
#endif

/* 启动定时中断任务 */
#define	sStartTask_TimInt(tmid)	\
	sUSV_StartTask(vTIMINT_THDL[tmid]->THDL, __SUSPENDED__)

/* 启动定时查询任务 */
#define	sStartTask_TimQry(tmid)	\
	sUSV_StartTask(vTIMQRY_THDL[tmid]->THDL, __SUSPENDED__)

/* 恢复定时中断任务 */
#define	sResumeTask_TimInt(tmid)	\
	sTSV_ResumeTask(sTaskNode(vTIMINT_THDL[tmid]->THDL))

/* 恢复定时查询任务 */
#define	sResumeTask_TimQry(tmid)	\
	sTSV_ResumeTask(sTaskNode(vTIMQRY_THDL[tmid]->THDL))

/* 设置任务优先级 */
#define	sSetPriority(node)	\
do{	\
	node->last->next = node->next;	\
	node->next->last = node->last;	\
	node->next = node->last = NULL;	\
	if(vTPLTAIL == NULL)	\
	{	\
		vTPLTAIL = node;	\
	}	\
	else	\
	{	\
		vTPLTAIL->next = node;	\
		node->last = vTPLTAIL;	\
		vTPLTAIL = node;	\
	}	\
}while(false)



/*
 * 私信 - 创建信箱
 */

#define m0__  __DM_VAR__

#if MCUCFG_DIRMSGTYPE == 0

#define __dm  static

#define sDMbox_Init	\
if(vSendDM_f)	\
{	\
	vSendDM_f = false;	\
	return;	\
}	\
else	\
{	\
	m0_ = false;	\
}

#define sCreateDMbox_0	\
(void)	\
{	\
	mEntry_Monitor;	\
	do

#define sCreateDMbox_1(m1)	\
(m0__, m1##_)	\
{	\
	__dm tDM m0;	\
	__dm m1;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_2(m1, m2)	\
(m0__, m1##_, m2##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_3(m1, m2, m3)	\
(m0__, m1##_, m2##_, m3##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_4(m1, m2, m3, m4)	\
(m0__, m1##_, m2##_, m3##_, m4##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_5(m1, m2, m3, m4, m5)	\
(m0__, m1##_, m2##_, m3##_, m4##_, m5##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_6(m1, m2, m3, m4, m5, m6)	\
(m0__, m1##_, m2##_, m3##_, m4##_, m5##_, m6##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_7(m1, m2, m3, m4, m5, m6, m7)	\
(m0__, m1##_, m2##_, m3##_, m4##_, m5##_, m6##_, m7##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_8(m1, m2, m3, m4, m5, m6, m7, m8)	\
(m0__, m1##_, m2##_, m3##_, m4##_, m5##_, m6##_, m7##_, m8##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_9(m1, m2, m3, m4, m5, m6, m7, m8, m9)	\
(m0__, m1##_, m2##_, m3##_, m4##_, m5##_, m6##_, m7##_, m8##_, m9##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_10(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10)	\
(m0__, m1##_, m2##_, m3##_, m4##_, m5##_, m6##_, m7##_, m8##_, m9##_, m10##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_11(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11)	\
(m0__, m1##_, m2##_, m3##_, m4##_, m5##_, m6##_, m7##_, m8##_, m9##_, m10##_, m11##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10; __dm m11;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_12(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12)	\
(m0__, m1##_, m2##_, m3##_, m4##_, m5##_, m6##_, m7##_, m8##_, m9##_, m10##_, m11##_, m12##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10; __dm m11; __dm m12;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_13(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13)	\
(m0__, m1##_, m2##_, m3##_, m4##_, m5##_, m6##_, m7##_, m8##_, m9##_, m10##_, m11##_, m12##_, m13##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10; __dm m11; __dm m12; __dm m13;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_14(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14)	\
(m0__, m1##_, m2##_, m3##_, m4##_, m5##_, m6##_, m7##_, m8##_, m9##_, m10##_, m11##_, m12##_, m13##_, m14##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10; __dm m11; __dm m12; __dm m13; __dm m14;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_15(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15)	\
(m0__, m1##_, m2##_, m3##_, m4##_, m5##_, m6##_, m7##_, m8##_, m9##_, m10##_, m11##_, m12##_, m13##_, m14##_, m15##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10; __dm m11; __dm m12; __dm m13; __dm m14; __dm m15;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_16(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16)	\
(m0__, m1##_, m2##_, m3##_, m4##_, m5##_, m6##_, m7##_, m8##_, m9##_, m10##_, m11##_, m12##_, m13##_, m14##_, m15##_, m16##_)	\
{	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10; __dm m11; __dm m12; __dm m13; __dm m14; __dm m15; __dm m16;	\
	__dm tDM mx;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#elif MCUCFG_DIRMSGTYPE == 1

#define __dm  __align(4) static

#define sDMbox_Init	\
if(vSendDM_f)	\
{	\
	register u32 *p;	\
	register u8 size;	\
	m0_ = true;	\
	p = &m0_ + 1;	\
	size = __DM_SIZE__;	\
	do{	\
		*p++ = *vDM_PSP++;	\
	}while(--size);	\
	vSendDM_f = false;	\
	return;	\
}

#define sCreateDMbox_0	\
(void)	\
{	\
	mEntry_Monitor;	\
	do

#define sCreateDMbox_1(m1)	\
(m0__, m1##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_;	\
	__dm tDM m0;	\
	__dm m1;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_2(m1, m2)	\
(m0__, m1##__, m2##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_; __dm m2##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_3(m1, m2, m3)	\
(m0__, m1##__, m2##__, m3##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_; __dm m2##_; __dm m3##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_4(m1, m2, m3, m4)	\
(m0__, m1##__, m2##__, m3##__, m4##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_; __dm m2##_; __dm m3##_; __dm m4##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_5(m1, m2, m3, m4, m5)	\
(m0__, m1##__, m2##__, m3##__, m4##__, m5##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_; __dm m2##_; __dm m3##_; __dm m4##_; __dm m5##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_6(m1, m2, m3, m4, m5, m6)	\
(m0__, m1##__, m2##__, m3##__, m4##__, m5##__, m6##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_; __dm m2##_; __dm m3##_; __dm m4##_; __dm m5##_; __dm m6##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_7(m1, m2, m3, m4, m5, m6, m7)	\
(m0__, m1##__, m2##__, m3##__, m4##__, m5##__, m6##__, m7##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_;	__dm m2##_; __dm m3##_; __dm m4##_; __dm m5##_; __dm m6##_; __dm m7##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_8(m1, m2, m3, m4, m5, m6, m7, m8)	\
(m0__, m1##__, m2##__, m3##__, m4##__, m5##__, m6##__, m7##__, m8##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_;	__dm m2##_; __dm m3##_; __dm m4##_; __dm m5##_; __dm m6##_; __dm m7##_; __dm m8##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_9(m1, m2, m3, m4, m5, m6, m7, m8, m9)	\
(m0__, m1##__, m2##__, m3##__, m4##__, m5##__, m6##__, m7##__, m8##__, m9##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_;	__dm m2##_; __dm m3##_; __dm m4##_; __dm m5##_; __dm m6##_; __dm m7##_; __dm m8##_; __dm m9##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_10(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10)	\
(m0__, m1##__, m2##__, m3##__, m4##__, m5##__, m6##__, m7##__, m8##__, m9##__, m10##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_;	__dm m2##_; __dm m3##_; __dm m4##_; __dm m5##_; __dm m6##_; __dm m7##_; __dm m8##_; __dm m9##_; __dm m10##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_11(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11)	\
(m0__, m1##__, m2##__, m3##__, m4##__, m5##__, m6##__, m7##__, m8##__, m9##__, m10##__, m11##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_;	__dm m2##_; __dm m3##_; __dm m4##_; __dm m5##_; __dm m6##_; __dm m7##_; __dm m8##_; __dm m9##_; __dm m10##_; __dm m11##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10; __dm m11;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_12(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12)	\
(m0__, m1##__, m2##__, m3##__, m4##__, m5##__, m6##__, m7##__, m8##__, m9##__, m10##__, m11##__, m12##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_;	__dm m2##_; __dm m3##_; __dm m4##_; __dm m5##_; __dm m6##_; __dm m7##_; __dm m8##_; __dm m9##_; __dm m10##_; __dm m11##_; __dm m12##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10; __dm m11; __dm m12;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_13(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13)	\
(m0__, m1##__, m2##__, m3##__, m4##__, m5##__, m6##__, m7##__, m8##__, m9##__, m10##__, m11##__, m12##__, m13##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_;	__dm m2##_; __dm m3##_; __dm m4##_; __dm m5##_; __dm m6##_; __dm m7##_; __dm m8##_; __dm m9##_; __dm m10##_; __dm m11##_; __dm m12##_; __dm m13##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10; __dm m11; __dm m12; __dm m13;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_14(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14)	\
(m0__, m1##__, m2##__, m3##__, m4##__, m5##__, m6##__, m7##__, m8##__, m9##__, m10##__, m11##__, m12##__, m13##__, m14##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_;	__dm m2##_; __dm m3##_; __dm m4##_; __dm m5##_; __dm m6##_; __dm m7##_; __dm m8##_; __dm m9##_; __dm m10##_; __dm m11##_; __dm m12##_; __dm m13##_; __dm m14##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10; __dm m11; __dm m12; __dm m13; __dm m14;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_15(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15)	\
(m0__, m1##__, m2##__, m3##__, m4##__, m5##__, m6##__, m7##__, m8##__, m9##__, m10##__, m11##__, m12##__, m13##__, m14##__, m15##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_;	__dm m2##_; __dm m3##_; __dm m4##_; __dm m5##_; __dm m6##_; __dm m7##_; __dm m8##_; __dm m9##_; __dm m10##_; __dm m11##_; __dm m12##_; __dm m13##_; __dm m14##_; __dm m15##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10; __dm m11; __dm m12; __dm m13; __dm m14; __dm m15;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#define sCreateDMbox_16(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16)	\
(m0__, m1##__, m2##__, m3##__, m4##__, m5##__, m6##__, m7##__, m8##__, m9##__, m10##__, m11##__, m12##__, m13##__, m14##__, m15##__, m16##__)	\
{	\
	__dm tDM m0_ = false;	\
	__dm m1##_;	__dm m2##_; __dm m3##_; __dm m4##_; __dm m5##_; __dm m6##_; __dm m7##_; __dm m8##_; __dm m9##_; __dm m10##_; __dm m11##_; __dm m12##_; __dm m13##_; __dm m14##_; __dm m15##_; __dm m16##_;	\
	__dm tDM m0;	\
	__dm m1; __dm m2; __dm m3; __dm m4; __dm m5; __dm m6; __dm m7; __dm m8; __dm m9; __dm m10; __dm m11; __dm m12; __dm m13; __dm m14; __dm m15; __dm m16;	\
	mEntry_Monitor;	\
	sDMbox_Init;	\
	do

#endif



/*
 * 事件标志组 - 写标志位（ISV）
 */

#define siWriteFlagBits(svid)	\
		isv_.value =	\
		(	\
			  sizeof(lv) == 1 ? *(u8  *)&lv	\
			: sizeof(lv) == 2 ? *(u16 *)&lv	\
			: sizeof(lv) == 4 ? *(u32 *)&lv	\
			: 0	\
		);	\
	}	\
	mISV_Do(svid);	\
}while(false)

#define siWriteFlagBits_1(svid, bit1)	\
	lv.bit1 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_2(svid, bit1, bit2)	\
	lv.bit1 = lv.bit2 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_3(svid, bit1, bit2, bit3)	\
	lv.bit1 = lv.bit2 = lv.bit3 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_4(svid, bit1, bit2, bit3, bit4)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_5(svid, bit1, bit2, bit3, bit4, bit5)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_6(svid, bit1, bit2, bit3, bit4, bit5, bit6)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_7(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_8(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_9(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_10(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_11(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_12(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_13(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_14(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_15(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_16(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_17(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_18(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_19(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_20(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_21(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20, bit21)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = lv.bit21 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_22(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20, bit21, bit22)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = lv.bit21 = lv.bit22 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_23(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20, bit21, bit22, bit23)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = lv.bit21 = lv.bit22 = lv.bit23 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_24(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20, bit21, bit22, bit23, bit24)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = lv.bit21 = lv.bit22 = lv.bit23 = lv.bit24 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_25(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20, bit21, bit22, bit23, bit24, bit25)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = lv.bit21 = lv.bit22 = lv.bit23 = lv.bit24 = lv.bit25 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_26(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20, bit21, bit22, bit23, bit24, bit25, bit26)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = lv.bit21 = lv.bit22 = lv.bit23 = lv.bit24 = lv.bit25 = lv.bit26 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_27(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20, bit21, bit22, bit23, bit24, bit25, bit26, bit27)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = lv.bit21 = lv.bit22 = lv.bit23 = lv.bit24 = lv.bit25 = lv.bit26 = lv.bit27 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_28(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20, bit21, bit22, bit23, bit24, bit25, bit26, bit27, bit28)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = lv.bit21 = lv.bit22 = lv.bit23 = lv.bit24 = lv.bit25 = lv.bit26 = lv.bit27 = lv.bit28 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_29(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20, bit21, bit22, bit23, bit24, bit25, bit26, bit27, bit28, bit29)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = lv.bit21 = lv.bit22 = lv.bit23 = lv.bit24 = lv.bit25 = lv.bit26 = lv.bit27 = lv.bit28 = lv.bit29 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_30(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20, bit21, bit22, bit23, bit24, bit25, bit26, bit27, bit28, bit29, bit30)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = lv.bit21 = lv.bit22 = lv.bit23 = lv.bit24 = lv.bit25 = lv.bit26 = lv.bit27 = lv.bit28 = lv.bit29 = lv.bit30 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_31(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20, bit21, bit22, bit23, bit24, bit25, bit26, bit27, bit28, bit29, bit30, bit31)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = lv.bit21 = lv.bit22 = lv.bit23 = lv.bit24 = lv.bit25 = lv.bit26 = lv.bit27 = lv.bit28 = lv.bit29 = lv.bit30 = lv.bit31 = true;	\
	siWriteFlagBits(svid)

#define siWriteFlagBits_32(svid, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8, bit9, bit10, bit11, bit12, bit13, bit14, bit15, bit16, bit17, bit18, bit19, bit20, bit21, bit22, bit23, bit24, bit25, bit26, bit27, bit28, bit29, bit30, bit31, bit32)	\
	lv.bit1 = lv.bit2 = lv.bit3 = lv.bit4 = lv.bit5 = lv.bit6 = lv.bit7 = lv.bit8 = lv.bit9 = lv.bit10 = lv.bit11 = lv.bit12 = lv.bit13 = lv.bit14 = lv.bit15 = lv.bit16 = lv.bit17 = lv.bit18 = lv.bit19 = lv.bit20 = lv.bit21 = lv.bit22 = lv.bit23 = lv.bit24 = lv.bit25 = lv.bit26 = lv.bit27 = lv.bit28 = lv.bit29 = lv.bit30 = lv.bit31 = lv.bit32 = true;	\
	siWriteFlagBits(svid)



/*
 * 消息队列
 */

#define sSendMsg_Dynamic(p)	\
do{	\
	msg_node->msg = p;	\
	msg_node->next = NULL;	\
	msg_node->last = ((tspMsgQueue_Dynamic)msg_queue)->msg_tail;   \
	if(((tspMsgQueue_Dynamic)msg_queue)->msg_tail != NULL)	\
	{	\
		((tspMsgQueue_Dynamic)msg_queue)->msg_tail->next = msg_node;	\
	}	\
	((tspMsgQueue_Dynamic)msg_queue)->msg_tail = msg_node;	\
	if(((tspMsgQueue_Dynamic)msg_queue)->msg_head == NULL)	\
	{	\
		((tspMsgQueue_Dynamic)msg_queue)->msg_head = msg_node;	\
	}	\
	msg_queue->counter++;	\
}while(false)

#define sRecvMsg_Dynamic(p)	\
do{	\
	if(msg_queue->mode == __MSGQUE_LIFO__)	\
	{	\
		msg_node = ((tspMsgQueue_Dynamic)msg_queue)->msg_tail;	\
		p = msg_node->msg;	\
		msg_node = msg_node->last;	\
		__free(((tspMsgQueue_Dynamic)msg_queue)->msg_tail);	\
		((tspMsgQueue_Dynamic)msg_queue)->msg_tail = msg_node;	\
		if(msg_node == NULL)	\
		{	\
			((tspMsgQueue_Dynamic)msg_queue)->msg_head = NULL;	\
		}	\
		else	\
		{	\
			msg_node->next = NULL;	\
		}	\
	}	\
	else	\
	{	\
		msg_node = ((tspMsgQueue_Dynamic)msg_queue)->msg_head;	\
		p = msg_node->msg;	\
		msg_node = msg_node->next;	\
		__free(((tspMsgQueue_Dynamic)msg_queue)->msg_head);	\
		((tspMsgQueue_Dynamic)msg_queue)->msg_head = msg_node;	\
		if(msg_node == NULL)	\
		{	\
			((tspMsgQueue_Dynamic)msg_queue)->msg_tail = NULL;	\
		}	\
		else	\
		{	\
			msg_node->last = NULL;	\
		}	\
	}	\
	msg_queue->counter--;	\
}while(false)

#define sSendMsg_Static(p)	\
do{	\
	if(((tspMsgQueue_Static)msg_queue)->msg_tail < ((tspMsgQueue_Static)msg_queue)->msg_base + msg_queue->len - 1)	\
	{	\
		((tspMsgQueue_Static)msg_queue)->msg_tail++;	\
	}	\
	else	\
	{	\
		((tspMsgQueue_Static)msg_queue)->msg_tail = ((tspMsgQueue_Static)msg_queue)->msg_base;	\
	}	\
	*((tspMsgQueue_Static)msg_queue)->msg_tail = p;	\
	msg_queue->counter++;	\
}while(false)

#define sRecvMsg_Static(p)	\
do{	\
	if(msg_queue->mode == __MSGQUE_LIFO__)	\
	{	\
		p = *((tspMsgQueue_Static)msg_queue)->msg_tail;	\
		if(((tspMsgQueue_Static)msg_queue)->msg_tail > ((tspMsgQueue_Static)msg_queue)->msg_base)	\
		{	\
			((tspMsgQueue_Static)msg_queue)->msg_tail--;	\
		}	\
		else	\
		{	\
			((tspMsgQueue_Static)msg_queue)->msg_tail = ((tspMsgQueue_Static)msg_queue)->msg_base + msg_queue->len - 1;	\
		}	\
	}	\
	else	\
	{	\
		p = *((tspMsgQueue_Static)msg_queue)->msg_head;	\
		if(((tspMsgQueue_Static)msg_queue)->msg_head < ((tspMsgQueue_Static)msg_queue)->msg_base + msg_queue->len - 1)	\
		{	\
			((tspMsgQueue_Static)msg_queue)->msg_head++;	\
		}	\
		else	\
		{	\
			((tspMsgQueue_Static)msg_queue)->msg_head = ((tspMsgQueue_Static)msg_queue)->msg_base;	\
		}	\
	}	\
	msg_queue->counter--;	\
}while(false)



#endif

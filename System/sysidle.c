/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     sysidle.c
 * @brief    系统空闲任务
 * @author   迟凯峰
 * @version  V2.3.0
 * @date     2023.04.25
 ******************************************************************************/

#include "syslink.h"

uCreateTask(Sysidle, 0, __STACKSIZE_SYSIDLE__, 0, 0)
{
	tTID i;
	for(i=1; i<OS_TASKTOTAL + 1; i++)
	{
		vOVERTIME[i] = false;
	}
	#if SYSCFG_SOFTRTC == __ENABLED__
	{
		static u8 year = 0xFF;
		if(year != vRealTime[0])
		{
			year = vRealTime[0];
			vMonth2Date = year ? ((year & 3) ? 28 : 29) : ((sDefCentury & 3) ? 28 : 29);
		}
	}
	#endif
	#if SYSCFG_IDLEHOOK == __ENABLED__
	idle_hook();
	#endif
	mSys_Idle;
	uEndTasking;
}

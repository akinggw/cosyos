/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     tsv.h
 * @brief    Service for SysTick_Handler(Tick-Hook, Timing Interrupt-Hook, Timing Query-Hook)
 * @author   迟凯峰
 * @version  V2.3.1
 * @date     2023.05.02
 ******************************************************************************/

#ifndef __TSV_H
#define __TSV_H

extern bool  send_msg__(tspMsgQueue msg_queue, void *msg);
extern void *recv_msg__(tspMsgQueue msg_queue);

/*
 * 任务
 */

/* 恢复指定任务 */
#define	sTSV_ResumeTask(node)	\
do{	\
	if(node->state & __SUSPENDED__) node->state &= __RESUME__;	\
}while(false)

/* 挂起指定任务 */
#define	sTSV_SuspendTask(node)	\
do{	\
	if(node->state < __SUSPENDED__) node->state |= __SUSPENDED__;	\
}while(false)

/* 删除指定任务 */
#define	sTSV_DeleteTask(node)	\
	node->state = __DELETED__

/* 设置任务优先级 */
#define	sTSV_SetPriority(node, tpl)	\
do{	\
	if(node->TPL == tpl) break;	\
	node->TPL = tpl;	\
	sSetPriority(node);	\
}while(false)



/*
 * 二值信号量
 */

/* 获取 */
#define sTSV_TakeBin(bin)	\
(	\
	bin ? bin-- : bin	\
)



/*
 * 计数信号量
 */

/* 获取 */
#define sTSV_TakeSem(sem)	\
(	\
	sem.counter ? sem.counter-- : sem.counter	\
)

/* 释放 */
#define sTSV_FreeSem(sem)	\
do{	\
	if(sem.counter < sem.max) sem.counter++;	\
}while(false)



/*
 * 定时
 */

/* 定时中断 */
#define	sTSV_TimInt(tmid, tc)	\
	vTIMINT_STMR[tmid] = vTIMINT_BUFF[tmid] = tc

/* 定时查询 */
#define	sTSV_TimQry(tmid, tc)	\
	vTIMQRY_STMR[tmid] = vTIMQRY_BUFF[tmid] = tc



/*
 * 私信
 */

/* 发送私信 */
#if SYSCFG_COMPILEMODE == __C89__
	#define	sTSV_SendDM(task)	\
		vSendDM_f = true;	\
		__DM_PSP__;	\
		task(__DM_VAL__, 
#elif SYSCFG_COMPILEMODE == __C99__
	#define sTSV_SendDM(task, ...)	\
	do{	\
		vSendDM_f = true;	\
		__DM_PSP__;	\
		task(__DM_VAL__, __VA_ARGS__);	\
	}while(false)
#endif



/*
 * 全局变量
 */

/* 写全局变量 */
#define sTSV_WriteGVar(gp, lp, size)	\
do{	\
	static tsVarNode _SV_MEM_ var_node = {gp, lp, size};	\
	sWRITE_VAR_WRITE(&var_node);	\
	size ? __memcpy(gp, lp, size) : __strcpy((char *)gp, (char *)lp);	\
	sWRITE_VAR_WRITE(NULL);	\
}while(false)

/* 全局变量自运算 */
#define sTSV_SelfOpe(gv, type, fp)	\
do{	\
	static type lv;	\
	static tsVarNode _SV_MEM_ var_node = {&gv, &lv, sizeof(gv)};	\
	lv = gv;	\
	sWRITE_VAR_WRITE(&var_node);	\
	(*fp)();	\
	sWRITE_VAR_WRITE(NULL);	\
}while(false)



/*
 * 消息邮箱
 */

/* 接收邮件 */
#define sTSV_RecvMail(mail, mbox)	\
(	\
	mbox##_gnmf	\
	? __memcpy(mail, &mbox, sizeof(mbox)) != NULL	\
		? mbox##_gnmf--	\
		: false	\
	: false	\
)

/* 发送邮件 */
#define sTSV_SendMail(mbox, mail)	\
do{	\
	static tsMbxNode _SV_MEM_ mbx_node = {&mbox, mail, sizeof(mbox), &mbox##_gnmf, true};	\
	mbx_node.lnmf = true;	\
	sWRITE_MBX_WRITE(&mbx_node);	\
	__memcpy(&mbox, mail, sizeof(mbox));	\
	sWRITE_MBX_WRITE(NULL);	\
	mbox##_gnmf = mbx_node.lnmf;	\
}while(false)



/*
 * 消息队列
 */

/* 发送消息 */
#define	sTSV_SendMsg(que, msg)	\
	send_msg__(que, msg)

/* 接收消息 */
#define	sTSV_RecvMsg(que)	\
	recv_msg__(que)



/*
 * 事件标志组
 */

/* 查询标志组 */
#define	sTSV_QueryFlagGroup(group)	\
(	\
	  sizeof(group) == 1 ? *(u8  *)&group ? true : false	\
	: sizeof(group) == 2 ? *(u16 *)&group ? true : false	\
	: sizeof(group) == 4 ? *(u32 *)&group ? true : false	\
	: false	\
)

/* 清除标志组 */
#define sTSV_ClearFlagGroup(group)	\
do{	\
	  sizeof(group) == 1 ? *(u8  *)&group = false	\
	: sizeof(group) == 2 ? *(u16 *)&group = false	\
	: sizeof(group) == 4 ? *(u32 *)&group = false	\
	: OS_NOPx1;	\
}while(false)



#endif

/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     isv_loc.c
 * @brief    Service for Interrupt is to be executed locally
 * @author   迟凯峰
 * @version  V2.3.1
 * @date     2023.05.02
 ******************************************************************************/

#include "glovar.h"
#include "sysapi.h"
#include "svs.h"

#if SYSCFG_MCUCORE == 8051
#pragma NOAREGS
#endif

#if SYSCFG_MAILBOX == __ENABLED__

bool __recv_mail__(void *lm, void *gm, size_t size, volatile bool *gf) __REENTRANT__
{
	if(OS_MBX_WRITE != NULL)
	{
		if(vMbxNode->gmbx == gm)
		{
			if(vMbxNode->lnmf)
			{
				__memcpy(lm, vMbxNode->lmbx, size);
				*gf = vMbxNode->lnmf = false;
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	if(*gf)
	{
		__memcpy(lm, gm, size);
		*gf = false;
		return true;
	}
	else
	{
		return false;
	}
}

#endif

#if SYSCFG_MSGQUEUE == __ENABLED__

void *__recv_msg__(tspMsgQueue msg_queue) __REENTRANT__
{
	void *msg = NULL;
	if(!msg_queue->mutex)
	{
		return NULL;
	}
	msg_queue->mutex = false;
	if(!msg_queue->counter);
	else if(msg_queue->type == __DYNAMIC__)
	{
		vFault.error_recv_msg_int = true;
	}
	else
	{
		sRecvMsg_Static(msg);
	}
	msg_queue->mutex = true;
	return msg;
}

#endif

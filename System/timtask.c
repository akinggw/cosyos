/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     timtask.c
 * @brief    定时任务
 * @author   迟凯峰
 * @version  V2.3.1
 * @date     2023.05.01
 ******************************************************************************/

#include "glovar.h"
#include "sysapi.h"

#define	EXT_TIMINT(tmid)	\
	extern tsTimIntHandle _TASK_MEM_ vTIMINT_THDL_##tmid;

#define	EXT_TIMQRY(tmid)	\
	extern tsTimQryHandle _TASK_MEM_ vTIMQRY_THDL_##tmid;

#define	INIT_TIMINT(tmid)	\
	vTIMINT_ARLD[tmid] = vTIMINT_THDL_##tmid.ARLD;	\
	vTIMINT_TYPE[tmid] = vTIMINT_THDL_##tmid.THDL != NULL ? true : false;	\
	vTIMINT_THDL[tmid] = &vTIMINT_THDL_##tmid;

#define	INIT_TIMQRY(tmid)	\
	vTIMQRY_ARLD[tmid] = vTIMQRY_THDL_##tmid.ARLD;	\
	vTIMQRY_TYPE[tmid] = vTIMQRY_THDL_##tmid.THDL != NULL ? true : false;	\
	vTIMQRY_THDL[tmid] = &vTIMQRY_THDL_##tmid;

#if OS_TIMINTTOTAL > 0
EXT_TIMINT(0)
#if OS_TIMINTTOTAL > 1
EXT_TIMINT(1)
#if OS_TIMINTTOTAL > 2
EXT_TIMINT(2)
#if OS_TIMINTTOTAL > 3
EXT_TIMINT(3)
#if OS_TIMINTTOTAL > 4
EXT_TIMINT(4)
#if OS_TIMINTTOTAL > 5
EXT_TIMINT(5)
#if OS_TIMINTTOTAL > 6
EXT_TIMINT(6)
#if OS_TIMINTTOTAL > 7
EXT_TIMINT(7)
#if OS_TIMINTTOTAL > 8
EXT_TIMINT(8)
#if OS_TIMINTTOTAL > 9
EXT_TIMINT(9)
#if OS_TIMINTTOTAL > 10
EXT_TIMINT(10)
#if OS_TIMINTTOTAL > 11
EXT_TIMINT(11)
#if OS_TIMINTTOTAL > 12
EXT_TIMINT(12)
#if OS_TIMINTTOTAL > 13
EXT_TIMINT(13)
#if OS_TIMINTTOTAL > 14
EXT_TIMINT(14)
#if OS_TIMINTTOTAL > 15
EXT_TIMINT(15)
#if OS_TIMINTTOTAL > 16
EXT_TIMINT(16)
#if OS_TIMINTTOTAL > 17
EXT_TIMINT(17)
#if OS_TIMINTTOTAL > 18
EXT_TIMINT(18)
#if OS_TIMINTTOTAL > 19
EXT_TIMINT(19)
#if OS_TIMINTTOTAL > 20
EXT_TIMINT(20)
#if OS_TIMINTTOTAL > 21
EXT_TIMINT(21)
#if OS_TIMINTTOTAL > 22
EXT_TIMINT(22)
#if OS_TIMINTTOTAL > 23
EXT_TIMINT(23)
#if OS_TIMINTTOTAL > 24
EXT_TIMINT(24)
#if OS_TIMINTTOTAL > 25
EXT_TIMINT(25)
#if OS_TIMINTTOTAL > 26
EXT_TIMINT(26)
#if OS_TIMINTTOTAL > 27
EXT_TIMINT(27)
#if OS_TIMINTTOTAL > 28
EXT_TIMINT(28)
#if OS_TIMINTTOTAL > 29
EXT_TIMINT(29)
#if OS_TIMINTTOTAL > 30
EXT_TIMINT(30)
#if OS_TIMINTTOTAL > 31
EXT_TIMINT(31)
#if OS_TIMINTTOTAL > 32
EXT_TIMINT(32)
#if OS_TIMINTTOTAL > 33
EXT_TIMINT(33)
#if OS_TIMINTTOTAL > 34
EXT_TIMINT(34)
#if OS_TIMINTTOTAL > 35
EXT_TIMINT(35)
#if OS_TIMINTTOTAL > 36
EXT_TIMINT(36)
#if OS_TIMINTTOTAL > 37
EXT_TIMINT(37)
#if OS_TIMINTTOTAL > 38
EXT_TIMINT(38)
#if OS_TIMINTTOTAL > 39
EXT_TIMINT(39)
#if OS_TIMINTTOTAL > 40
EXT_TIMINT(40)
#if OS_TIMINTTOTAL > 41
EXT_TIMINT(41)
#if OS_TIMINTTOTAL > 42
EXT_TIMINT(42)
#if OS_TIMINTTOTAL > 43
EXT_TIMINT(43)
#if OS_TIMINTTOTAL > 44
EXT_TIMINT(44)
#if OS_TIMINTTOTAL > 45
EXT_TIMINT(45)
#if OS_TIMINTTOTAL > 46
EXT_TIMINT(46)
#if OS_TIMINTTOTAL > 47
EXT_TIMINT(47)
#if OS_TIMINTTOTAL > 48
EXT_TIMINT(48)
#if OS_TIMINTTOTAL > 49
EXT_TIMINT(49)
#if OS_TIMINTTOTAL > 50
EXT_TIMINT(50)
#if OS_TIMINTTOTAL > 51
EXT_TIMINT(51)
#if OS_TIMINTTOTAL > 52
EXT_TIMINT(52)
#if OS_TIMINTTOTAL > 53
EXT_TIMINT(53)
#if OS_TIMINTTOTAL > 54
EXT_TIMINT(54)
#if OS_TIMINTTOTAL > 55
EXT_TIMINT(55)
#if OS_TIMINTTOTAL > 56
EXT_TIMINT(56)
#if OS_TIMINTTOTAL > 57
EXT_TIMINT(57)
#if OS_TIMINTTOTAL > 58
EXT_TIMINT(58)
#if OS_TIMINTTOTAL > 59
EXT_TIMINT(59)
#if OS_TIMINTTOTAL > 60
EXT_TIMINT(60)
#if OS_TIMINTTOTAL > 61
EXT_TIMINT(61)
#if OS_TIMINTTOTAL > 62
EXT_TIMINT(62)
#if OS_TIMINTTOTAL > 63
EXT_TIMINT(63)
#if OS_TIMINTTOTAL > 64
EXT_TIMINT(64)
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif

#if OS_TIMQRYTOTAL > 0
EXT_TIMQRY(0)
#if OS_TIMQRYTOTAL > 1
EXT_TIMQRY(1)
#if OS_TIMQRYTOTAL > 2
EXT_TIMQRY(2)
#if OS_TIMQRYTOTAL > 3
EXT_TIMQRY(3)
#if OS_TIMQRYTOTAL > 4
EXT_TIMQRY(4)
#if OS_TIMQRYTOTAL > 5
EXT_TIMQRY(5)
#if OS_TIMQRYTOTAL > 6
EXT_TIMQRY(6)
#if OS_TIMQRYTOTAL > 7
EXT_TIMQRY(7)
#if OS_TIMQRYTOTAL > 8
EXT_TIMQRY(8)
#if OS_TIMQRYTOTAL > 9
EXT_TIMQRY(9)
#if OS_TIMQRYTOTAL > 10
EXT_TIMQRY(10)
#if OS_TIMQRYTOTAL > 11
EXT_TIMQRY(11)
#if OS_TIMQRYTOTAL > 12
EXT_TIMQRY(12)
#if OS_TIMQRYTOTAL > 13
EXT_TIMQRY(13)
#if OS_TIMQRYTOTAL > 14
EXT_TIMQRY(14)
#if OS_TIMQRYTOTAL > 15
EXT_TIMQRY(15)
#if OS_TIMQRYTOTAL > 16
EXT_TIMQRY(16)
#if OS_TIMQRYTOTAL > 17
EXT_TIMQRY(17)
#if OS_TIMQRYTOTAL > 18
EXT_TIMQRY(18)
#if OS_TIMQRYTOTAL > 19
EXT_TIMQRY(19)
#if OS_TIMQRYTOTAL > 20
EXT_TIMQRY(20)
#if OS_TIMQRYTOTAL > 21
EXT_TIMQRY(21)
#if OS_TIMQRYTOTAL > 22
EXT_TIMQRY(22)
#if OS_TIMQRYTOTAL > 23
EXT_TIMQRY(23)
#if OS_TIMQRYTOTAL > 24
EXT_TIMQRY(24)
#if OS_TIMQRYTOTAL > 25
EXT_TIMQRY(25)
#if OS_TIMQRYTOTAL > 26
EXT_TIMQRY(26)
#if OS_TIMQRYTOTAL > 27
EXT_TIMQRY(27)
#if OS_TIMQRYTOTAL > 28
EXT_TIMQRY(28)
#if OS_TIMQRYTOTAL > 29
EXT_TIMQRY(29)
#if OS_TIMQRYTOTAL > 30
EXT_TIMQRY(30)
#if OS_TIMQRYTOTAL > 31
EXT_TIMQRY(31)
#if OS_TIMQRYTOTAL > 32
EXT_TIMQRY(32)
#if OS_TIMQRYTOTAL > 33
EXT_TIMQRY(33)
#if OS_TIMQRYTOTAL > 34
EXT_TIMQRY(34)
#if OS_TIMQRYTOTAL > 35
EXT_TIMQRY(35)
#if OS_TIMQRYTOTAL > 36
EXT_TIMQRY(36)
#if OS_TIMQRYTOTAL > 37
EXT_TIMQRY(37)
#if OS_TIMQRYTOTAL > 38
EXT_TIMQRY(38)
#if OS_TIMQRYTOTAL > 39
EXT_TIMQRY(39)
#if OS_TIMQRYTOTAL > 40
EXT_TIMQRY(40)
#if OS_TIMQRYTOTAL > 41
EXT_TIMQRY(41)
#if OS_TIMQRYTOTAL > 42
EXT_TIMQRY(42)
#if OS_TIMQRYTOTAL > 43
EXT_TIMQRY(43)
#if OS_TIMQRYTOTAL > 44
EXT_TIMQRY(44)
#if OS_TIMQRYTOTAL > 45
EXT_TIMQRY(45)
#if OS_TIMQRYTOTAL > 46
EXT_TIMQRY(46)
#if OS_TIMQRYTOTAL > 47
EXT_TIMQRY(47)
#if OS_TIMQRYTOTAL > 48
EXT_TIMQRY(48)
#if OS_TIMQRYTOTAL > 49
EXT_TIMQRY(49)
#if OS_TIMQRYTOTAL > 50
EXT_TIMQRY(50)
#if OS_TIMQRYTOTAL > 51
EXT_TIMQRY(51)
#if OS_TIMQRYTOTAL > 52
EXT_TIMQRY(52)
#if OS_TIMQRYTOTAL > 53
EXT_TIMQRY(53)
#if OS_TIMQRYTOTAL > 54
EXT_TIMQRY(54)
#if OS_TIMQRYTOTAL > 55
EXT_TIMQRY(55)
#if OS_TIMQRYTOTAL > 56
EXT_TIMQRY(56)
#if OS_TIMQRYTOTAL > 57
EXT_TIMQRY(57)
#if OS_TIMQRYTOTAL > 58
EXT_TIMQRY(58)
#if OS_TIMQRYTOTAL > 59
EXT_TIMQRY(59)
#if OS_TIMQRYTOTAL > 60
EXT_TIMQRY(60)
#if OS_TIMQRYTOTAL > 61
EXT_TIMQRY(61)
#if OS_TIMQRYTOTAL > 62
EXT_TIMQRY(62)
#if OS_TIMQRYTOTAL > 63
EXT_TIMQRY(63)
#if OS_TIMQRYTOTAL > 64
EXT_TIMQRY(64)
#if OS_TIMQRYTOTAL > 65
EXT_TIMQRY(65)
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif

void __init_timtask(void)
{
	u8 i;
	#if OS_TIMINTTOTAL > 0
	for(i=0; i<OS_TIMINTTOTAL; i++)
	{
		vTIMINT_STMR[i] = vTIMINT_BUFF[i] = 0;
	}
	INIT_TIMINT(0)
	#if OS_TIMINTTOTAL > 1
	INIT_TIMINT(1)
	#if OS_TIMINTTOTAL > 2
	INIT_TIMINT(2)
	#if OS_TIMINTTOTAL > 3
	INIT_TIMINT(3)
	#if OS_TIMINTTOTAL > 4
	INIT_TIMINT(4)
	#if OS_TIMINTTOTAL > 5
	INIT_TIMINT(5)
	#if OS_TIMINTTOTAL > 6
	INIT_TIMINT(6)
	#if OS_TIMINTTOTAL > 7
	INIT_TIMINT(7)
	#if OS_TIMINTTOTAL > 8
	INIT_TIMINT(8)
	#if OS_TIMINTTOTAL > 9
	INIT_TIMINT(9)
	#if OS_TIMINTTOTAL > 10
	INIT_TIMINT(10)
	#if OS_TIMINTTOTAL > 11
	INIT_TIMINT(11)
	#if OS_TIMINTTOTAL > 12
	INIT_TIMINT(12)
	#if OS_TIMINTTOTAL > 13
	INIT_TIMINT(13)
	#if OS_TIMINTTOTAL > 14
	INIT_TIMINT(14)
	#if OS_TIMINTTOTAL > 15
	INIT_TIMINT(15)
	#if OS_TIMINTTOTAL > 16
	INIT_TIMINT(16)
	#if OS_TIMINTTOTAL > 17
	INIT_TIMINT(17)
	#if OS_TIMINTTOTAL > 18
	INIT_TIMINT(18)
	#if OS_TIMINTTOTAL > 19
	INIT_TIMINT(19)
	#if OS_TIMINTTOTAL > 20
	INIT_TIMINT(20)
	#if OS_TIMINTTOTAL > 21
	INIT_TIMINT(21)
	#if OS_TIMINTTOTAL > 22
	INIT_TIMINT(22)
	#if OS_TIMINTTOTAL > 23
	INIT_TIMINT(23)
	#if OS_TIMINTTOTAL > 24
	INIT_TIMINT(24)
	#if OS_TIMINTTOTAL > 25
	INIT_TIMINT(25)
	#if OS_TIMINTTOTAL > 26
	INIT_TIMINT(26)
	#if OS_TIMINTTOTAL > 27
	INIT_TIMINT(27)
	#if OS_TIMINTTOTAL > 28
	INIT_TIMINT(28)
	#if OS_TIMINTTOTAL > 29
	INIT_TIMINT(29)
	#if OS_TIMINTTOTAL > 30
	INIT_TIMINT(30)
	#if OS_TIMINTTOTAL > 31
	INIT_TIMINT(31)
	#if OS_TIMINTTOTAL > 32
	INIT_TIMINT(32)
	#if OS_TIMINTTOTAL > 33
	INIT_TIMINT(33)
	#if OS_TIMINTTOTAL > 34
	INIT_TIMINT(34)
	#if OS_TIMINTTOTAL > 35
	INIT_TIMINT(35)
	#if OS_TIMINTTOTAL > 36
	INIT_TIMINT(36)
	#if OS_TIMINTTOTAL > 37
	INIT_TIMINT(37)
	#if OS_TIMINTTOTAL > 38
	INIT_TIMINT(38)
	#if OS_TIMINTTOTAL > 39
	INIT_TIMINT(39)
	#if OS_TIMINTTOTAL > 40
	INIT_TIMINT(40)
	#if OS_TIMINTTOTAL > 41
	INIT_TIMINT(41)
	#if OS_TIMINTTOTAL > 42
	INIT_TIMINT(42)
	#if OS_TIMINTTOTAL > 43
	INIT_TIMINT(43)
	#if OS_TIMINTTOTAL > 44
	INIT_TIMINT(44)
	#if OS_TIMINTTOTAL > 45
	INIT_TIMINT(45)
	#if OS_TIMINTTOTAL > 46
	INIT_TIMINT(46)
	#if OS_TIMINTTOTAL > 47
	INIT_TIMINT(47)
	#if OS_TIMINTTOTAL > 48
	INIT_TIMINT(48)
	#if OS_TIMINTTOTAL > 49
	INIT_TIMINT(49)
	#if OS_TIMINTTOTAL > 50
	INIT_TIMINT(50)
	#if OS_TIMINTTOTAL > 51
	INIT_TIMINT(51)
	#if OS_TIMINTTOTAL > 52
	INIT_TIMINT(52)
	#if OS_TIMINTTOTAL > 53
	INIT_TIMINT(53)
	#if OS_TIMINTTOTAL > 54
	INIT_TIMINT(54)
	#if OS_TIMINTTOTAL > 55
	INIT_TIMINT(55)
	#if OS_TIMINTTOTAL > 56
	INIT_TIMINT(56)
	#if OS_TIMINTTOTAL > 57
	INIT_TIMINT(57)
	#if OS_TIMINTTOTAL > 58
	INIT_TIMINT(58)
	#if OS_TIMINTTOTAL > 59
	INIT_TIMINT(59)
	#if OS_TIMINTTOTAL > 60
	INIT_TIMINT(60)
	#if OS_TIMINTTOTAL > 61
	INIT_TIMINT(61)
	#if OS_TIMINTTOTAL > 62
	INIT_TIMINT(62)
	#if OS_TIMINTTOTAL > 63
	INIT_TIMINT(63)
	#if OS_TIMINTTOTAL > 64
	INIT_TIMINT(64)
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	
	#if OS_TIMQRYTOTAL > 0
	for(i=0; i<OS_TIMQRYTOTAL; i++)
	{
		vTIMQRY_STMR[i] = vTIMQRY_BUFF[i] = 0;
	}
	INIT_TIMQRY(0)
	#if OS_TIMQRYTOTAL > 1
	INIT_TIMQRY(1)
	#if OS_TIMQRYTOTAL > 2
	INIT_TIMQRY(2)
	#if OS_TIMQRYTOTAL > 3
	INIT_TIMQRY(3)
	#if OS_TIMQRYTOTAL > 4
	INIT_TIMQRY(4)
	#if OS_TIMQRYTOTAL > 5
	INIT_TIMQRY(5)
	#if OS_TIMQRYTOTAL > 6
	INIT_TIMQRY(6)
	#if OS_TIMQRYTOTAL > 7
	INIT_TIMQRY(7)
	#if OS_TIMQRYTOTAL > 8
	INIT_TIMQRY(8)
	#if OS_TIMQRYTOTAL > 9
	INIT_TIMQRY(9)
	#if OS_TIMQRYTOTAL > 10
	INIT_TIMQRY(10)
	#if OS_TIMQRYTOTAL > 11
	INIT_TIMQRY(11)
	#if OS_TIMQRYTOTAL > 12
	INIT_TIMQRY(12)
	#if OS_TIMQRYTOTAL > 13
	INIT_TIMQRY(13)
	#if OS_TIMQRYTOTAL > 14
	INIT_TIMQRY(14)
	#if OS_TIMQRYTOTAL > 15
	INIT_TIMQRY(15)
	#if OS_TIMQRYTOTAL > 16
	INIT_TIMQRY(16)
	#if OS_TIMQRYTOTAL > 17
	INIT_TIMQRY(17)
	#if OS_TIMQRYTOTAL > 18
	INIT_TIMQRY(18)
	#if OS_TIMQRYTOTAL > 19
	INIT_TIMQRY(19)
	#if OS_TIMQRYTOTAL > 20
	INIT_TIMQRY(20)
	#if OS_TIMQRYTOTAL > 21
	INIT_TIMQRY(21)
	#if OS_TIMQRYTOTAL > 22
	INIT_TIMQRY(22)
	#if OS_TIMQRYTOTAL > 23
	INIT_TIMQRY(23)
	#if OS_TIMQRYTOTAL > 24
	INIT_TIMQRY(24)
	#if OS_TIMQRYTOTAL > 25
	INIT_TIMQRY(25)
	#if OS_TIMQRYTOTAL > 26
	INIT_TIMQRY(26)
	#if OS_TIMQRYTOTAL > 27
	INIT_TIMQRY(27)
	#if OS_TIMQRYTOTAL > 28
	INIT_TIMQRY(28)
	#if OS_TIMQRYTOTAL > 29
	INIT_TIMQRY(29)
	#if OS_TIMQRYTOTAL > 30
	INIT_TIMQRY(30)
	#if OS_TIMQRYTOTAL > 31
	INIT_TIMQRY(31)
	#if OS_TIMQRYTOTAL > 32
	INIT_TIMQRY(32)
	#if OS_TIMQRYTOTAL > 33
	INIT_TIMQRY(33)
	#if OS_TIMQRYTOTAL > 34
	INIT_TIMQRY(34)
	#if OS_TIMQRYTOTAL > 35
	INIT_TIMQRY(35)
	#if OS_TIMQRYTOTAL > 36
	INIT_TIMQRY(36)
	#if OS_TIMQRYTOTAL > 37
	INIT_TIMQRY(37)
	#if OS_TIMQRYTOTAL > 38
	INIT_TIMQRY(38)
	#if OS_TIMQRYTOTAL > 39
	INIT_TIMQRY(39)
	#if OS_TIMQRYTOTAL > 40
	INIT_TIMQRY(40)
	#if OS_TIMQRYTOTAL > 41
	INIT_TIMQRY(41)
	#if OS_TIMQRYTOTAL > 42
	INIT_TIMQRY(42)
	#if OS_TIMQRYTOTAL > 43
	INIT_TIMQRY(43)
	#if OS_TIMQRYTOTAL > 44
	INIT_TIMQRY(44)
	#if OS_TIMQRYTOTAL > 45
	INIT_TIMQRY(45)
	#if OS_TIMQRYTOTAL > 46
	INIT_TIMQRY(46)
	#if OS_TIMQRYTOTAL > 47
	INIT_TIMQRY(47)
	#if OS_TIMQRYTOTAL > 48
	INIT_TIMQRY(48)
	#if OS_TIMQRYTOTAL > 49
	INIT_TIMQRY(49)
	#if OS_TIMQRYTOTAL > 50
	INIT_TIMQRY(50)
	#if OS_TIMQRYTOTAL > 51
	INIT_TIMQRY(51)
	#if OS_TIMQRYTOTAL > 52
	INIT_TIMQRY(52)
	#if OS_TIMQRYTOTAL > 53
	INIT_TIMQRY(53)
	#if OS_TIMQRYTOTAL > 54
	INIT_TIMQRY(54)
	#if OS_TIMQRYTOTAL > 55
	INIT_TIMQRY(55)
	#if OS_TIMQRYTOTAL > 56
	INIT_TIMQRY(56)
	#if OS_TIMQRYTOTAL > 57
	INIT_TIMQRY(57)
	#if OS_TIMQRYTOTAL > 58
	INIT_TIMQRY(58)
	#if OS_TIMQRYTOTAL > 59
	INIT_TIMQRY(59)
	#if OS_TIMQRYTOTAL > 60
	INIT_TIMQRY(60)
	#if OS_TIMQRYTOTAL > 61
	INIT_TIMQRY(61)
	#if OS_TIMQRYTOTAL > 62
	INIT_TIMQRY(62)
	#if OS_TIMQRYTOTAL > 63
	INIT_TIMQRY(63)
	#if OS_TIMQRYTOTAL > 64
	INIT_TIMQRY(64)
	#if OS_TIMQRYTOTAL > 65
	INIT_TIMQRY(65)
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	vTIMQRY_STMR[__TMID_TASKMGR__] =
	vTIMQRY_BUFF[__TMID_TASKMGR__] =
	(1000UL * SYSCFG_TASKMGRUPSPEED) / SYSCFG_STKCYCLE;
}

/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     isv_iss.h
 * @brief    Service for Interrupt is to be executed in ISS(Interrupt Service Stack)
 * @author   迟凯峰
 * @version  V2.3.2
 * @date     2023.04.30
 ******************************************************************************/

#ifndef __ISV_ISS_H
#define __ISV_ISS_H

/*
 * 任务
 */

#if SYSCFG_TASKCREATEMODE == __STATIC__ || SYSCFG_TASKCREATEMODE == __BALANCE__

/* 恢复任务 */
#define	sISV_ResumeTask(node, svid)	\
do{	\
	static tsSV_OpeTask _SV_MEM_ isv_ = {__SVID_RESTASK__, node};	\
	mISV_Do(svid);	\
}while(false)

/* 挂起任务 */
#define	sISV_SuspendTask(node, svid)	\
do{	\
	static tsSV_OpeTask _SV_MEM_ isv_ = {__SVID_SUSTASK__, node};	\
	mISV_Do(svid);	\
}while(false)

/* 删除任务 */
#define	sISV_DeleteTask(node, svid)	\
do{	\
	static tsSV_OpeTask _SV_MEM_ isv_ = {__SVID_DELTASK__, node};	\
	mISV_Do(svid);	\
}while(false)

/* 设置任务优先级 */
#define	sISV_SetPriority(node, tpl, svid)	\
do{	\
	static tsSV_SetTPL _SV_MEM_ isv_ = {__SVID_SETTPL__, node, tpl};	\
	mISV_Do(svid);	\
}while(false)

#elif SYSCFG_TASKCREATEMODE == __DYNAMIC__

/* 恢复任务 */
#define	sISV_ResumeTask(node, svid)	\
do{	\
	static tsSV_OpeTask _SV_MEM_ isv_ = {__SVID_RESTASK__, NULL};	\
	isv_.task_node = node;	\
	mISV_Do(svid);	\
}while(false)

/* 挂起任务 */
#define	sISV_SuspendTask(node, svid)	\
do{	\
	static tsSV_OpeTask _SV_MEM_ isv_ = {__SVID_SUSTASK__, NULL};	\
	isv_.task_node = node;	\
	mISV_Do(svid);	\
}while(false)

/* 删除任务 */
#define	sISV_DeleteTask(node, svid)	\
do{	\
	static tsSV_OpeTask _SV_MEM_ isv_ = {__SVID_DELTASK__, NULL};	\
	isv_.task_node = node;	\
	mISV_Do(svid);	\
}while(false)

/* 设置任务优先级 */
#define	sISV_SetPriority(node, tpl, svid)	\
do{	\
	static tsSV_SetTPL _SV_MEM_ isv_ = {__SVID_SETTPL__, NULL, tpl};	\
	isv_.task_node = node;	\
	mISV_Do(svid);	\
}while(false)

#endif



/*
 * 计数信号量
 */

/* 释放 */
#define sISV_FreeSem(sem, svid)	\
do{	\
	static tsSV_Sem _SV_MEM_ isv_ = {__SVID_FREESEM__, &sem};	\
	mISV_Do(svid);	\
}while(false)



/*
 * 定时
 */

/* 定时中断 */
#define	sISV_TimInt(tmid, tc, svid)	\
do{	\
	static tsSV_TimInt _SV_MEM_ isv_ = {__SVID_TIMINT__, tmid, tc};	\
	mISV_Do(svid);	\
}while(false)

/* 定时查询 */
#define	sISV_TimQry(tmid, tc, svid)	\
do{	\
	static tsSV_TimQry _SV_MEM_ isv_ = {__SVID_TIMQRY__, tmid, tc};	\
	mISV_Do(svid);	\
}while(false)



/*
 * 消息邮箱
 */

/* 发送邮件 */
#define sISV_SendMail(mbox, mail, svid)	\
do{	\
	static tsMbxNode _SV_MEM_ mbx_node = {&mbox, mail, sizeof(mbox), &mbox##_gnmf, true};	\
	static tsSV_Mbx  _SV_MEM_ isv_ = {__SVID_SENDMAIL__, &mbx_node};	\
	mISV_Do(svid);	\
}while(false)



/*
 * 消息队列
 */

/* 发送消息 */
#define	sISV_SendMsg(que, msg, svid)	\
do{	\
	static tsSV_Msg  _SV_MEM_ isv_ = {__SVID_SENDMSG__, que, msg};	\
	mISV_Do(svid);	\
}while(false)



/*
 * 事件标志组
 */

/* 清除标志组 */
#define sISV_ClearFlagGroup(group, svid)	\
do{	\
	static tsSV_GRP _SV_MEM_ isv_ = {__SVID_WRITEGRP__, (volatile void *)&group, -sizeof(group), ~0};	\
	mISV_Do(svid);	\
}while(false)

/* 写多标志位 */
#define sISV_WriteFlagBits(group, sign, nbit, svid)	\
do{	\
	static tsSV_GRP _SV_MEM_ isv_ = {__SVID_WRITEGRP__, (volatile void *)&group, sign##sizeof(group), 0};	\
	struct ts_##group lv;	\
    miWriteFlagBits;	\
	  sizeof(lv) == 1 ? *(u8  *)&lv = false	\
	: sizeof(lv) == 2 ? *(u16 *)&lv = false	\
	: sizeof(lv) == 4 ? *(u32 *)&lv = false	\
	: OS_NOPx1;	\
	siWriteFlagBits_##nbit(svid, 



/*
 * 全局变量
 */

/* 全局变量写访问 */
#define sISV_WriteGVar(gp, lp, size, svid)	\
do{	\
	static tsVarNode _SV_MEM_ var_node = {gp, lp, size};	\
	static tsSV_GVar _SV_MEM_ isv_ = {__SVID_WRITEVAR__, &var_node};	\
	mISV_Do(svid);	\
}while(false)

/* 全局变量自运算 */
#define sISV_SelfOpe(gv, type, fp, svid)	\
do{	\
	static type lv;	\
	static tsVarNode _SV_MEM_ var_node = {&gv, &lv, sizeof(gv)};	\
	static tsSV_SelfOpe _SV_MEM_ isv_ = {__SVID_SELFOPE__, &var_node, fp};	\
	mISV_Do(svid);	\
}while(false)



#endif

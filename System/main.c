/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     main.c
 * @brief    主函数
 * @author   迟凯峰
 * @version  V2.3.0
 * @date     2023.04.25
 ******************************************************************************/

#include "syslink.h"

int main(void)
{
	tTID i;
	extern void __init_timtask(void);
	__init_timtask();
	for(i=0; i<OS_TASKTOTAL + 1; i++)
	{
		vOVERTIME[i] = vDELAY_STMR[i] = 0;
	}
	init_hook();
	mSys_INIT;
	sStartTask_TimQry(__TMID_TASKMGR__);
	uStartTask(Starter, __READY__);
	uStartTask(Sysidle, __READY__);
	i = OS_TIMINTTOTAL;
	while(i--)
	{
		if(vTIMINT_TYPE[i])
		{
			sStartTask_TimInt(i);
		}
	}
	i = OS_TIMQRYTOTAL - 1;
	while(i--)
	{
		if(vTIMQRY_TYPE[i])
		{
			sStartTask_TimQry(i);
		}
	}
	vRET_f = false;
	vScheduling_f = true;
	mPSV_Trigger;
	while(true);
}

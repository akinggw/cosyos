/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     csv.h
 * @brief    Service for Create Projects
 * @author   迟凯峰
 * @version  V2.3.1
 * @date     2023.05.02
 ******************************************************************************/

#ifndef __CSV_H
#define __CSV_H

/*
 * 声明任务
 */

/* 无私信 */
#define sCSV_ExternTask(name)	\
	extern tsTaskHandle _THDL_MEM_ vTaskHandle_##name;	\
	extern void name(void)

/* 有私信 */
#if SYSCFG_COMPILEMODE == __C89__
	#define sCSV_ExternTask_DM(name)	\
		extern tsTaskHandle _THDL_MEM_ vTaskHandle_##name;	\
		extern void name(__DM_VAR__, 
#elif SYSCFG_COMPILEMODE == __C99__
	#define sCSV_ExternTask_DM(name, ...)	\
		extern tsTaskHandle _THDL_MEM_ vTaskHandle_##name;	\
		extern void name(__DM_VAR__, __VA_ARGS__)
#endif



/*
 * 创建任务
 */

/* 创建一般任务 */
#if SYSCFG_TASKCREATEMODE == __STATIC__
	#define sCSV_CreateTask(name, tpl, tss, srt, ndm)	\
		__BSP_ALIGN__ static u8 _TASK_MEM_ vTaskStack_##name[tss];	\
		tsTaskHandle _THDL_MEM_ vTaskHandle_##name = {0, 0, 0, 0, 0, NULL, vTaskStack_##name, tss, #name, {0}, __BASICSTACKSIZE__, srt, 0, NULL, NULL, tpl, (tfpVV)name};	\
		void name	\
		sCreateDMbox_##ndm
#elif SYSCFG_TASKCREATEMODE == __BALANCE__
	#define sCSV_CreateTask(name, tpl, tss, srt, ndm)	\
		tsTaskHandle _THDL_MEM_ vTaskHandle_##name = {0, 0, 0, 0, 0, NULL, NULL, tss, #name, {0}, __BASICSTACKSIZE__, srt, 0, NULL, NULL, tpl, (tfpVV)name, false};	\
		void name	\
		sCreateDMbox_##ndm
#elif SYSCFG_TASKCREATEMODE == __DYNAMIC__
	#define sCSV_CreateTask(name, tpl, tss, srt, ndm)	\
		tsTaskHandle _THDL_MEM_ vTaskHandle_##name = {0, tss, tpl, (tfpVV)name, #name, srt, NULL};	\
		void name	\
		sCreateDMbox_##ndm
#endif

/* 创建定时中断任务 */
#define sCSV_CreateTask_TimInt(tmid, arl, name, tpl, tss, srt, ndm)	\
	tsTaskHandle _THDL_MEM_ vTaskHandle_##name;	\
	tsTimIntHandle _TASK_MEM_ vTIMINT_THDL_##tmid = {&vTaskHandle_##name, (tfpVV)0, arl};	\
	sCSV_CreateTask(name, tpl, tss, srt, ndm)

/* 创建定时查询任务 */
#define sCSV_CreateTask_TimQry(tmid, event, arl, name, tpl, tss, srt, ndm)	\
	bool fTIMQRY_EVENT_##tmid(void) __C51USING__	\
	{	\
		return (event ? true : false);	\
	}	\
	tsTaskHandle _THDL_MEM_ vTaskHandle_##name;	\
	tsTimQryHandle _TASK_MEM_ vTIMQRY_THDL_##tmid = {&vTaskHandle_##name, (tfpVV)0, (tfpBV)fTIMQRY_EVENT_##tmid, arl};	\
	sCSV_CreateTask(name, tpl, tss, srt, ndm)



/*
 * 创建钩子
 */

/* 创建定时中断钩子 */
#define sCSV_CreateHook_TimInt(tmid, arl, name)	\
	void name(void);	\
	tsTimIntHandle _TASK_MEM_ vTIMINT_THDL_##tmid = {(tspTaskHandle)0, (tfpVV)name, arl};	\
	void name(void) __USING__

/* 创建定时查询钩子 */
#define sCSV_CreateHook_TimQry(tmid, event, arl, name)	\
	bool fTIMQRY_EVENT_##tmid(void) __C51USING__	\
	{	\
		return (event ? true : false);	\
	}	\
	void name(void);	\
	tsTimQryHandle _TASK_MEM_ vTIMQRY_THDL_##tmid = {(tspTaskHandle)0, (tfpVV)name, (tfpBV)fTIMQRY_EVENT_##tmid, arl};	\
	void name(void) __USING__



/*
 * 二值信号量
 */

/* 声明 */
#define	sCSV_ExternBin(name)	\
	extern volatile bool name

/* 创建 */
#define	sCSV_CreateBin(name)	\
	volatile bool name



/*
 * 互斥信号量
 */

/* 声明 */
#define	sCSV_ExternMut(name)	\
	extern tsMut name

/* 创建 */
#define	sCSV_CreateMut(name)	\
	tsMut name = {true, 0, 0, 0}



/*
 * 计数信号量
 */

/* 声明 */
#define	sCSV_ExternSem(name)	\
	extern tsSem name

/* 创建 */
#define	sCSV_CreateSem(name, init, max)	\
	tsSem name = {init, max}



/*
 * 消息邮箱
 */

/* 声明 */
#define sCSV_ExternMailbox(name, type)	\
	extern type name;	\
	sCSV_ExternBin(name##_gnmf)

/* 创建 */
#define sCSV_CreateMailbox(name, type)	\
	type name;	\
	sCSV_CreateBin(name##_gnmf) = false



/*
 * 消息队列
 */

/* 声明静态消息队列 */
#define sCSV_ExternMsgQueue_Static(name)	\
	extern tsMsgQueue_Static name

/* 声明动态消息队列 */
#define sCSV_ExternMsgQueue_Dynamic(name)	\
	extern tsMsgQueue_Dynamic name

/* 创建静态消息队列 */
#define	sCSV_CreateMsgQueue_Static(name, mode, len)	\
	void *vMsgQueue_##name[len];	\
	tsMsgQueue_Static name = {__STATIC__, mode, len, 0, true, vMsgQueue_##name, vMsgQueue_##name + len - 1, vMsgQueue_##name}

/* 创建动态消息队列 */
#define	sCSV_CreateMsgQueue_Dynamic(name, mode, len)	\
	tsMsgQueue_Dynamic name = {__DYNAMIC__, mode, len, 0, true, NULL, NULL}



/*
 * 事件标志组
 */

/* 声明 */
#define sCSV_ExternFlagGroup(name)	\
	extern volatile struct ts_##name name;	\
	typedef struct ts_##name

/* 创建 */
#define sCSV_CreateFlagGroup(name)	\
	volatile struct ts_##name name



/*
 * 线程内存
 */

/* 创建线程内存池 */
#define	sCSV_CreateMempool	\
	tsThrmem thrmem = {NULL, NULL, 0}



#endif

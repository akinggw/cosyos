/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     isv_loc.h
 * @brief    Service for Interrupt is to be executed locally
 * @author   迟凯峰
 * @version  V2.3.0
 * @date     2023.04.25
 ******************************************************************************/

#ifndef __ISV_LOC_H
#define __ISV_LOC_H

extern bool __recv_mail__(void *lm, void *gm, size_t size, volatile bool *gf) __REENTRANT__;
extern void *__recv_msg__(tspMsgQueue msg_queue) __REENTRANT__;

/*
 * 二值信号量
 */

/* 获取 */
#define sISV_TakeBin(bin)	\
(	\
	bin ? bin-- : bin	\
)



/*
 * 全局变量
 */

/* 读全局变量 */
#define sISV_ReadGVar(gv, type)	\
(	\
	OS_VAR_WRITE != NULL	\
	? vVarNode->gvar == (void *)&gv	\
		? *(type *)vVarNode->lvar	\
		: gv	\
	: gv	\
)

/* 读全局数组 */
#define sISV_ReadGAry(lp, gp, size)	\
__memcpy(lp,	\
	OS_VAR_WRITE != NULL	\
	? vVarNode->gvar == (void *)gp	\
		? vVarNode->lvar	\
		: gp	\
	: gp,	\
	size	\
)

/* 读全局字符串 */
#define sISV_ReadGStr(ls, gs)	\
__strcpy(ls,	\
	OS_VAR_WRITE != NULL	\
	? vVarNode->gvar == (void *)gs	\
		? (char *)vVarNode->lvar	\
		: gs	\
	: gs	\
)



/*
 * 消息邮箱
 */

/* 接收邮件 */
#define sISV_RecvMail(mail, mbox)	\
	__recv_mail__(mail, &mbox, sizeof(mbox), &mbox##_gnmf)



/*
 * 消息队列
 */

/* 接收消息 */
#define	sISV_RecvMsg(que)	\
	__recv_msg__(que)



/*
 * 事件标志组
 */

 /* 查询标志组 */
#define	sISV_QueryFlagGroup(group)	\
(	\
	  sizeof(group) == 1 ? *(u8  *)&group ? true : false	\
	: sizeof(group) == 2 ? *(u16 *)&group ? true : false	\
	: sizeof(group) == 4 ? *(u32 *)&group ? true : false	\
	: false	\
)



#endif

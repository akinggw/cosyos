/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     svs.h
 * @brief    中断异步服务
 * @author   迟凯峰
 * @version  V2.3.1
 * @date     2023.05.02
 ******************************************************************************/

#ifndef __SVS_H
#define __SVS_H

#if MCUCFG_MCULEVEL == __FEIPAOZI__
#define WRITE_VAR_WRITE_START  os_sign_write_var_write = true
#define WRITE_VAR_WRITE_END    os_sign_write_var_write = false
#define WRITE_MBX_WRITE_START  os_sign_write_mbx_write = true
#define WRITE_MBX_WRITE_END    os_sign_write_mbx_write = false
#else
#define WRITE_VAR_WRITE_START  do{}while(false)
#define WRITE_VAR_WRITE_END    do{}while(false)
#define WRITE_MBX_WRITE_START  do{}while(false)
#define WRITE_MBX_WRITE_END    do{}while(false)
#endif

#define sWRITE_VAR_WRITE(p)	\
	WRITE_VAR_WRITE_START;	\
	vVarNode = p;	\
	WRITE_VAR_WRITE_END

#define sWRITE_MBX_WRITE(p)	\
	WRITE_MBX_WRITE_START;	\
	vMbxNode = p;	\
	WRITE_MBX_WRITE_END

/* 恢复任务 */
#define CASE_RESTASK	\
case __SVID_RESTASK__:	\
{	\
	tspTaskNode task_node = ((tspSV_OpeTask)svs)->task_node;	\
	if(task_node->state & __SUSPENDED__)	\
	{	\
		task_node->state &= __RESUME__;	\
		if(!vScheduling_f)	\
		{	\
			if(task_node->state <= __FLOATING__ && task_node->TPL > vTASKING->TPL)	\
			{	\
				vScheduling_f = true;	\
			}	\
		}	\
	}	\
}	\
break;

/* 挂起任务 */
#define CASE_SUSTASK	\
case __SVID_SUSTASK__:	\
{	\
	tspTaskNode task_node = ((tspSV_OpeTask)svs)->task_node;	\
	if(task_node->state < __SUSPENDED__)	\
	{	\
		task_node->state |= __SUSPENDED__;	\
		if(!vScheduling_f)	\
		{	\
			if(task_node == vTASKING)	\
			{	\
				vScheduling_f = true;	\
			}	\
		}	\
	}	\
}	\
break;

/* 删除任务 */
#define CASE_DELTASK	\
case __SVID_DELTASK__:	\
{	\
	tspTaskNode task_node = ((tspSV_OpeTask)svs)->task_node;	\
	task_node->state = __DELETED__;	\
	if(!vScheduling_f)	\
	{	\
		if(task_node == vTASKING)	\
		{	\
			vScheduling_f = true;	\
		}	\
	}	\
}	\
break;

/* 设置优先级 */
#define CASE_SETTPL	\
case __SVID_SETTPL__:	\
{	\
	tspTaskNode node_curr = ((tspSV_SetTPL)svs)->task_node;	\
	u8 tpl = ((tspSV_SetTPL)svs)->TPL;	\
	if(node_curr->TPL == tpl) break;	\
	node_curr->TPL = tpl;	\
	sSetPriority(node_curr);	\
	if(!vScheduling_f)	\
	{	\
		if(tpl > vTASKING->TPL && node_curr->state <= __FLOATING__)	\
		{	\
			vScheduling_f = true;	\
		}	\
	}	\
}	\
break;

/* 定时中断 */
#define CASE_TIMINT	\
case __SVID_TIMINT__:	\
{	\
	vTIMINT_STMR[((tspSV_TimInt)svs)->tmid] =	\
	vTIMINT_BUFF[((tspSV_TimInt)svs)->tmid] = ((tspSV_TimInt)svs)->tcyc;	\
}	\
break;

/* 定时查询 */
#define CASE_TIMQRY	\
case __SVID_TIMQRY__:	\
{	\
	vTIMQRY_STMR[((tspSV_TimQry)svs)->tmid] =	\
	vTIMQRY_BUFF[((tspSV_TimQry)svs)->tmid] = ((tspSV_TimQry)svs)->tcyc;	\
}	\
break;

/* 释放信号量 */
#define CASE_FREESEM	\
case __SVID_FREESEM__:	\
{	\
	if(((tspSV_Sem)svs)->sem->counter < ((tspSV_Sem)svs)->sem->max)	\
	{	\
		((tspSV_Sem)svs)->sem->counter++;	\
	}	\
}	\
break;

/* 全局变量写 */
#define CASE_WRITEVAR	\
case __SVID_WRITEVAR__:	\
{	\
	tspVarNode var_node = ((tspSV_GVar)svs)->var_node;	\
	sWRITE_VAR_WRITE(var_node);	\
	var_node->var_size	\
	? __memcpy(var_node->gvar, var_node->lvar, var_node->var_size)	\
	: __strcpy((char *)var_node->gvar, (char *)var_node->lvar);	\
	sWRITE_VAR_WRITE(NULL);	\
}	\
break;

/* 全局变量自运算 */
#define CASE_SELFOPE	\
case __SVID_SELFOPE__:	\
{	\
	tspVarNode var_node = ((tspSV_SelfOpe)svs)->var_node;	\
	__memcpy(var_node->lvar, var_node->gvar, var_node->var_size);	\
	sWRITE_VAR_WRITE(var_node);	\
	(*((tspSV_SelfOpe)svs)->fp)();	\
	sWRITE_VAR_WRITE(NULL);	\
}	\
break;

/* 发送邮件 */
#if SYSCFG_MAILBOX == __ENABLED__
#define CASE_SENDMAIL	\
case __SVID_SENDMAIL__:	\
{	\
	tspMbxNode mbx_node = ((tspSV_Mbx)svs)->mbx_node;	\
	mbx_node->lnmf = true;	\
	sWRITE_MBX_WRITE(mbx_node);	\
	__memcpy(mbx_node->gmbx, mbx_node->lmbx, mbx_node->mbx_size);	\
	sWRITE_MBX_WRITE(NULL);	\
	*mbx_node->gnmf = mbx_node->lnmf;	\
}	\
break;
#else
#define CASE_SENDMAIL
#endif

/* 发送消息 */
#if SYSCFG_MSGQUEUE == __ENABLED__
#define CASE_SENDMSG	\
case __SVID_SENDMSG__:	\
{	\
	tspMsgQueue msg_queue = ((tspSV_Msg)svs)->msg_queue;	\
	msg_queue->mutex = false;	\
	if(msg_queue->counter == msg_queue->len)	\
	{	\
		vAlarm.overflow_msg_queue = true;	\
		msg_queue->mutex = true;	\
		break;	\
	}	\
	if(msg_queue->type == __DYNAMIC__)	\
	{	\
		tspMsgNode msg_node = NULL;	\
		msg_node = (tspMsgNode)__malloc(sizeof(tsMsgNode));	\
		if(msg_node == NULL)	\
		{	\
			vFault.malloc_fail_send_msg = true;	\
			msg_queue->mutex = true;	\
			break;	\
		}	\
		sSendMsg_Dynamic(((tspSV_Msg)svs)->msg);	\
	}	\
	else	\
	{	\
		sSendMsg_Static(((tspSV_Msg)svs)->msg);	\
	}	\
	msg_queue->mutex = true;	\
}	\
break;
#else
#define CASE_SENDMSG
#endif

/* 写标志组 */
#if SYSCFG_FLAGGROUP == __ENABLED__
#define CASE_WRITEGRP	\
case __SVID_WRITEGRP__:	\
{	\
	switch(((tspSV_GRP)svs)->size)	\
	{	\
		case +1: *(u8  *)((tspSV_GRP)svs)->group |= ((tspSV_GRP)svs)->value; break;	\
		case +2: *(u16 *)((tspSV_GRP)svs)->group |= ((tspSV_GRP)svs)->value; break;	\
		case +4: *(u32 *)((tspSV_GRP)svs)->group |= ((tspSV_GRP)svs)->value; break;	\
		case -1: *(u8  *)((tspSV_GRP)svs)->group &=~((tspSV_GRP)svs)->value; break;	\
		case -2: *(u16 *)((tspSV_GRP)svs)->group &=~((tspSV_GRP)svs)->value; break;	\
		case -4: *(u32 *)((tspSV_GRP)svs)->group &=~((tspSV_GRP)svs)->value; break;	\
	}	\
}	\
break;
#else
#define CASE_WRITEGRP
#endif

/* ISS_Handler */
#define sISS_Handler	\
switch(*(const u8 _SV_MEM_ *)svs)	\
{	\
	CASE_RESTASK	\
	CASE_SUSTASK	\
	CASE_DELTASK	\
	CASE_SETTPL	\
	CASE_TIMINT	\
	CASE_TIMQRY	\
	CASE_FREESEM	\
	CASE_WRITEVAR	\
	CASE_SELFOPE	\
	CASE_SENDMAIL	\
	CASE_SENDMSG	\
	CASE_WRITEGRP	\
}



#endif

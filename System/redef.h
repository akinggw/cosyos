/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     redef.h
 * @brief    系统重定义
 * @author   迟凯峰
 * @version  V2.3.1
 * @date     2023.05.02
 ******************************************************************************/

#ifndef __REDEF_H
#define __REDEF_H
#include <stdlib.h>
#include "..\Config\syscfg.h"
#include sReDefStr(sReCatStr(..\Config\mcucfg_, SYSCFG_MCUCORE.h))

/*------------------------------------------------------------------------------
 * SYS-API */

#define sInitRealTime                         sInitRealTime1(SYSCFG_MANUFACTUREDATE)
#define sInitRealTime1(d)                     sInitRealTime2(d)
#define sInitRealTime2(y, m, d, H, M, s, w)   {y % 100, m, d, H, M, s, w}

#define sDefCentury                           sDefCentury1(SYSCFG_MANUFACTUREDATE)
#define sDefCentury1(d)                       sDefCentury2(d)
#define sDefCentury2(y, m, d, H, M, s, w)     (y % 100 ? y / 100 + 1 : y / 100)

/*------------------------------------------------------------------------------
 * OS_NOPxX */

#define OS_NOPx1  SYSCFG_NOP
#define OS_NOPx2  SYSCFG_NOP; SYSCFG_NOP
#define OS_NOPx3  SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP
#define OS_NOPx4  SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP
#define OS_NOPx5  SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP
#define OS_NOPx6  SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP
#define OS_NOPx7  SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP
#define OS_NOPx8  SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP; SYSCFG_NOP
#define OS_NOPxX  sReCatStr(OS_NOPx, SYSCFG_NOPxX)

/*------------------------------------------------------------------------------
 * TOTAL */

#define OS_TASKTOTAL      (SYSCFG_USERTASKTOTAL + 4)

#define OS_TIMINTTOTAL    (SYSCFG_USERTIMINTTOTAL + 1)
#define OS_TIMQRYTOTAL    (SYSCFG_USERTIMQRYTOTAL + 2)
#if   OS_TIMQRYTOTAL == 2
#define __TMID_TASKMGR__  1
#elif OS_TIMQRYTOTAL == 3
#define __TMID_TASKMGR__  2
#elif OS_TIMQRYTOTAL == 4
#define __TMID_TASKMGR__  3
#elif OS_TIMQRYTOTAL == 5
#define __TMID_TASKMGR__  4
#elif OS_TIMQRYTOTAL == 6
#define __TMID_TASKMGR__  5
#elif OS_TIMQRYTOTAL == 7
#define __TMID_TASKMGR__  6
#elif OS_TIMQRYTOTAL == 8
#define __TMID_TASKMGR__  7
#elif OS_TIMQRYTOTAL == 9
#define __TMID_TASKMGR__  8
#elif OS_TIMQRYTOTAL == 10
#define __TMID_TASKMGR__  9
#elif OS_TIMQRYTOTAL == 11
#define __TMID_TASKMGR__  10
#elif OS_TIMQRYTOTAL == 12
#define __TMID_TASKMGR__  11
#elif OS_TIMQRYTOTAL == 13
#define __TMID_TASKMGR__  12
#elif OS_TIMQRYTOTAL == 14
#define __TMID_TASKMGR__  13
#elif OS_TIMQRYTOTAL == 15
#define __TMID_TASKMGR__  14
#elif OS_TIMQRYTOTAL == 16
#define __TMID_TASKMGR__  15
#elif OS_TIMQRYTOTAL == 17
#define __TMID_TASKMGR__  16
#elif OS_TIMQRYTOTAL == 18
#define __TMID_TASKMGR__  17
#elif OS_TIMQRYTOTAL == 19
#define __TMID_TASKMGR__  18
#elif OS_TIMQRYTOTAL == 20
#define __TMID_TASKMGR__  19
#elif OS_TIMQRYTOTAL == 21
#define __TMID_TASKMGR__  20
#elif OS_TIMQRYTOTAL == 22
#define __TMID_TASKMGR__  21
#elif OS_TIMQRYTOTAL == 23
#define __TMID_TASKMGR__  22
#elif OS_TIMQRYTOTAL == 24
#define __TMID_TASKMGR__  23
#elif OS_TIMQRYTOTAL == 25
#define __TMID_TASKMGR__  24
#elif OS_TIMQRYTOTAL == 26
#define __TMID_TASKMGR__  25
#elif OS_TIMQRYTOTAL == 27
#define __TMID_TASKMGR__  26
#elif OS_TIMQRYTOTAL == 28
#define __TMID_TASKMGR__  27
#elif OS_TIMQRYTOTAL == 29
#define __TMID_TASKMGR__  28
#elif OS_TIMQRYTOTAL == 30
#define __TMID_TASKMGR__  29
#elif OS_TIMQRYTOTAL == 31
#define __TMID_TASKMGR__  30
#elif OS_TIMQRYTOTAL == 32
#define __TMID_TASKMGR__  31
#elif OS_TIMQRYTOTAL == 33
#define __TMID_TASKMGR__  32
#elif OS_TIMQRYTOTAL == 34
#define __TMID_TASKMGR__  33
#elif OS_TIMQRYTOTAL == 35
#define __TMID_TASKMGR__  34
#elif OS_TIMQRYTOTAL == 36
#define __TMID_TASKMGR__  35
#elif OS_TIMQRYTOTAL == 37
#define __TMID_TASKMGR__  36
#elif OS_TIMQRYTOTAL == 38
#define __TMID_TASKMGR__  37
#elif OS_TIMQRYTOTAL == 39
#define __TMID_TASKMGR__  38
#elif OS_TIMQRYTOTAL == 40
#define __TMID_TASKMGR__  39
#elif OS_TIMQRYTOTAL == 41
#define __TMID_TASKMGR__  40
#elif OS_TIMQRYTOTAL == 42
#define __TMID_TASKMGR__  41
#elif OS_TIMQRYTOTAL == 43
#define __TMID_TASKMGR__  42
#elif OS_TIMQRYTOTAL == 44
#define __TMID_TASKMGR__  43
#elif OS_TIMQRYTOTAL == 45
#define __TMID_TASKMGR__  44
#elif OS_TIMQRYTOTAL == 46
#define __TMID_TASKMGR__  45
#elif OS_TIMQRYTOTAL == 47
#define __TMID_TASKMGR__  46
#elif OS_TIMQRYTOTAL == 48
#define __TMID_TASKMGR__  47
#elif OS_TIMQRYTOTAL == 49
#define __TMID_TASKMGR__  48
#elif OS_TIMQRYTOTAL == 50
#define __TMID_TASKMGR__  49
#elif OS_TIMQRYTOTAL == 51
#define __TMID_TASKMGR__  50
#elif OS_TIMQRYTOTAL == 52
#define __TMID_TASKMGR__  51
#elif OS_TIMQRYTOTAL == 53
#define __TMID_TASKMGR__  52
#elif OS_TIMQRYTOTAL == 54
#define __TMID_TASKMGR__  53
#elif OS_TIMQRYTOTAL == 55
#define __TMID_TASKMGR__  54
#elif OS_TIMQRYTOTAL == 56
#define __TMID_TASKMGR__  55
#elif OS_TIMQRYTOTAL == 57
#define __TMID_TASKMGR__  56
#elif OS_TIMQRYTOTAL == 58
#define __TMID_TASKMGR__  57
#elif OS_TIMQRYTOTAL == 59
#define __TMID_TASKMGR__  58
#elif OS_TIMQRYTOTAL == 60
#define __TMID_TASKMGR__  59
#elif OS_TIMQRYTOTAL == 61
#define __TMID_TASKMGR__  60
#elif OS_TIMQRYTOTAL == 62
#define __TMID_TASKMGR__  61
#elif OS_TIMQRYTOTAL == 63
#define __TMID_TASKMGR__  62
#elif OS_TIMQRYTOTAL == 64
#define __TMID_TASKMGR__  63
#elif OS_TIMQRYTOTAL == 65
#define __TMID_TASKMGR__  64
#elif OS_TIMQRYTOTAL == 66
#define __TMID_TASKMGR__  65
#endif

#if SYSCFG_TIMESHARINGMODE == 0
#define OS_TIMESHARING     SYSCFG_GLOBALTIMESHARING
#define OS_MAXTIMESHARING  SYSCFG_GLOBALTIMESHARING
#define OS_MINTIMESHARING  SYSCFG_GLOBALTIMESHARING
#elif SYSCFG_TIMESHARINGMODE == 1
#define OS_TIMESHARING     SYSCFG_ALGORITHMTIMESHARING(vTASKING->TPL)
#define OS_MAXTIMESHARING  SYSCFG_ALGORITHMTIMESHARING(0)
#define OS_MINTIMESHARING  SYSCFG_ALGORITHMTIMESHARING(SYSCFG_TASKPRIORITY - 1)
#elif SYSCFG_TIMESHARINGMODE == 2
#define OS_TIMESHARING     vTIMESHARING[vTASKING->TPL]
#define OS_MAXTIMESHARING  SYSCFG_MAXTIMESHARING
#define OS_MINTIMESHARING  1
#endif
#if OS_MINTIMESHARING == 0 || OS_MAXTIMESHARING > 65535
#error 非法的设置值！
#endif

/*------------------------------------------------------------------------------
 * Taskmgr - LEN */

#if   OS_TASKTOTAL <= 999
#define __TIDLEN__  6
#elif OS_TASKTOTAL <= 9999
#define __TIDLEN__  7
#elif OS_TASKTOTAL <= 99999
#define __TIDLEN__  8
#elif OS_TASKTOTAL <= 999999
#define __TIDLEN__  9
#endif

#define __SYSALMLINE__      5

#if MCUCFG_MCUARC == __ARM__
#define	__RAMLEN__          15
#if MCUCFG_SVSTACKMONITOR == __ENABLED__
#define __SVSLEN__          (5 + 3 + (3 * 2 + 1)) /* (Head + Tail + Len) */
#define	__SYSFLTLINE__      7
#else
#define __SVSLEN__          0
#define	__SYSFLTLINE__      6
#endif
#else
#define	__RAMLEN__          13
#define __SVSLEN__          0
#define	__SYSFLTLINE__      6
#endif

#if SYSCFG_TASKPCMONITOR == __ENABLED__
#define __TPCLEN__          (9 + 3 + 8) /* (Head + Tail + Len) */
#else
#define __TPCLEN__          0
#endif

#if SYSCFG_STKTIMECOUNT == __ENABLED__
#define	__SYSTICKLEN__      (9 + 5 + 7) /* (Head + Tail + Len) */
#else
#define	__SYSTICKLEN__      0
#endif

#define __SYSALMLEN__       (7 + 3 - 2 + 5 * __SYSALMLINE__) /* (Head + Tail - Back + Len * Line) */
#define	__SYSFLTLEN__       (7 + 3 - 2 + 5 * __SYSFLTLINE__) /* (Head + Tail - Back + Len * Line) */

/*------------------------------------------------------------------------------
 * DEBUG_BUFF - SIZE */

#define OS_DEBUGRECVBUFFSIZE    (SYSCFG_TASKNAMEMAXLEN + 64)
#define OS_CMDLINESENDBUFFSIZE  (SYSCFG_TASKNAMEMAXLEN + 64)
#define OS_TASKMGRSENDBUFFSIZE	\
(	\
	(SYSCFG_TASKNAMEMAXLEN + __TIDLEN__ + __TPLLEN__ + __STALEN__ + __CPULEN__ + __RAMLEN__ + 2) * (OS_TASKTOTAL + 2)	\
	+ (2 * 3)	\
	+ __SVSLEN__	\
	+ __TPCLEN__	\
	+ __SYSTICKLEN__	\
	+ __SYSALMLEN__	\
	+ __SYSFLTLEN__	\
	+ 1	\
)

/*------------------------------------------------------------------------------
 * data type */

#if OS_MAXTIMESHARING < 256
typedef u8  tTimeSharing;
#elif OS_MAXTIMESHARING < 65536
typedef u16 tTimeSharing;
#else
#error 时间片溢出！
#endif

#if   OS_TASKTOTAL + 1 < 256
typedef u8  tTID;
#elif OS_TASKTOTAL + 1 < 65536
typedef u16 tTID;
#elif OS_TASKTOTAL + 1 < 4294967296
typedef u32 tTID;
#else
typedef u64 tTID;
#endif

typedef u8  tTaskQueueLen;

#define tSOFTTIMER2(p)  u##p
#define tSOFTTIMER1(p)  tSOFTTIMER2(p)
#define tDelay          tSOFTTIMER1(SYSCFG_DELAYBITS)
#define tTimInt         tSOFTTIMER1(SYSCFG_TIMINTBITS)
#define tTimQry         tSOFTTIMER1(SYSCFG_TIMQRYBITS)
#define tSemSize        tSOFTTIMER1(SYSCFG_SEMAPHOREBITS)

typedef void (_CODE_MEM_ *tfpVV)(void);
typedef bool (_CODE_MEM_ *tfpBV)(void);

/*------------------------------------------------------------------------------
 * typedef struct */

typedef struct
{
	sDefBitField(year);
	sDefBitField(month);
	sDefBitField(day);
	sDefBitField(hour);
	sDefBitField(minute);
	sDefBitField(second);
	sDefVoidBits(2);
}tsEvery;

typedef struct
{
	sDefBitField(realloc_task_stack);
	sDefBitField(overflow_task_queue);
	sDefBitField(overflow_task_priority);
	sDefBitField(overflow_msg_queue);
	sDefBitField(overtime_saferuntime);
	sDefVoidBits(3);
}tsAlarm;

typedef struct
{
	sDefBitField(malloc_fail_send_msg);
	sDefBitField(malloc_fail_task_node);
	sDefBitField(malloc_fail_task_stack);
	sDefBitField(realloc_fail_task_stack);
	sDefBitField(overflow_task_stack);
	sDefBitField(error_recv_msg_int);
	sDefBitField(overflow_iss);
	sDefVoidBits(1);
}tsFault;

typedef struct
{
	void _MALLOC_MEM_ *p1;
	void _MALLOC_MEM_ *p2;
	size_t size;
}tsThrmem;

typedef struct
{
	volatile tSemSize counter;
	const tSemSize max;
}tsSem;
typedef tsSem *tspSem;

typedef struct
{
	void *gvar;
	void *lvar;
	size_t var_size;
}tsVarNode;
typedef tsVarNode _SV_MEM_ *tspVarNode;

typedef struct
{
	void *gmbx;
	void *lmbx;
	size_t mbx_size;
	volatile bool *gnmf;
	volatile bool  lnmf;
}tsMbxNode;
typedef tsMbxNode _SV_MEM_ *tspMbxNode;

typedef	struct tsMsgNode
{
	void *msg;
	struct tsMsgNode _MALLOC_MEM_ *last;
	struct tsMsgNode _MALLOC_MEM_ *next;
}tsMsgNode;
typedef	tsMsgNode _MALLOC_MEM_ *tspMsgNode;

typedef	struct
{
	const u8 type;
	const u8 mode;
	const size_t len;
	size_t counter;
	volatile bool mutex;
}tsMsgQueue;
typedef	tsMsgQueue *tspMsgQueue;

typedef	struct
{
	const u8 type;
	const u8 mode;
	const size_t len;
	size_t counter;
	volatile bool mutex;
	void **msg_head;
	void **msg_tail;
	void **msg_base;
}tsMsgQueue_Static;
typedef	tsMsgQueue_Static *tspMsgQueue_Static;

typedef	struct
{
	const u8 type;
	const u8 mode;
	const size_t len;
	size_t counter;
	volatile bool mutex;
	tspMsgNode msg_head;
	tspMsgNode msg_tail;
}tsMsgQueue_Dynamic;
typedef	tsMsgQueue_Dynamic *tspMsgQueue_Dynamic;

typedef struct tsTaskNode
{
	mTaskNode_Head_
	tTID TID;
	u8 TPL;
	u8 state;
	u16 blocktype;
	void *ptr;
	u8 _MALLOC_MEM_ *BSP;
	tStackSize stacksize;
	const char _CONST_MEM_ *NAME;
	u32 usedtime[2];
	tStackSize stacklen_max;
	u16 saferuntime;
	u32 counter;
	struct tsTaskNode _MALLOC_MEM_ *last;
	struct tsTaskNode _MALLOC_MEM_ *next;
	#if SYSCFG_TASKCREATEMODE == __STATIC__ || SYSCFG_TASKCREATEMODE == __BALANCE__
	u8 TPL0;
	tfpVV entry;
	#endif
	#if SYSCFG_TASKCREATEMODE == __BALANCE__ || SYSCFG_TASKCREATEMODE == __DYNAMIC__
	bool realloc;
	#endif
	mTaskNode_Tail_
}tsTaskNode;
typedef tsTaskNode _MALLOC_MEM_ *tspTaskNode;

#if SYSCFG_TASKCREATEMODE == __STATIC__ || SYSCFG_TASKCREATEMODE == __BALANCE__
typedef tsTaskNode tsTaskHandle;
#elif SYSCFG_TASKCREATEMODE == __DYNAMIC__
typedef	struct
{
	tTID  TID;
	const tStackSize stacksize;
	const u8 TPL0;
	tfpVV entry;
	const char _CONST_MEM_ *NAME;
	u16 saferuntime;
	tspTaskNode task_node;
}tsTaskHandle;
#endif
typedef	tsTaskHandle _THDL_MEM_ *tspTaskHandle;

typedef	struct
{
	tspTaskHandle THDL;
	tfpVV HOOK;
	bool ARLD;
}tsTimIntHandle;
typedef	tsTimIntHandle _TASK_MEM_ *tspTimIntHandle;

typedef	struct
{
	tspTaskHandle THDL;
	tfpVV HOOK;
	tfpBV EVENT;
	bool ARLD;
}tsTimQryHandle;
typedef	tsTimQryHandle _TASK_MEM_ *tspTimQryHandle;

typedef struct
{
	volatile bool mut;
	tspTaskNode node;
	u8 TPL;
	u8 tpl;
}tsMut;
typedef tsMut *tspMut;

typedef struct
{
	const u8 svid;
	tspTaskNode task_node;
}tsSV_OpeTask;
typedef tsSV_OpeTask _SV_MEM_ *tspSV_OpeTask;

typedef struct
{
	const u8 svid;
	tspTaskNode task_node;
	const u8 TPL;
}tsSV_SetTPL;
typedef tsSV_SetTPL _SV_MEM_ *tspSV_SetTPL;

typedef struct
{
	const u8 svid;
	const u8 tmid;
	tTimInt  tcyc;
}tsSV_TimInt;
typedef tsSV_TimInt _SV_MEM_ *tspSV_TimInt;

typedef struct
{
	const u8 svid;
	const u8 tmid;
	tTimQry  tcyc;
}tsSV_TimQry;
typedef tsSV_TimQry _SV_MEM_ *tspSV_TimQry;

typedef struct
{
	const u8 svid;
	tspSem sem;
}tsSV_Sem;
typedef tsSV_Sem _SV_MEM_ *tspSV_Sem;

typedef struct
{
	const u8 svid;
	tspVarNode var_node;
}tsSV_GVar;
typedef tsSV_GVar _SV_MEM_ *tspSV_GVar;

typedef struct
{
	const u8 svid;
	tspVarNode var_node;
	tfpVV fp;
}tsSV_SelfOpe;
typedef tsSV_SelfOpe _SV_MEM_ *tspSV_SelfOpe;

typedef struct
{
	const u8 svid;
	tspMbxNode mbx_node;
}tsSV_Mbx;
typedef tsSV_Mbx _SV_MEM_ *tspSV_Mbx;

typedef struct
{
	const u8 svid;
	tspMsgQueue msg_queue;
	void *msg;
}tsSV_Msg;
typedef tsSV_Msg _SV_MEM_ *tspSV_Msg;

typedef struct
{
	const u8 svid;
	volatile void *group;
	const s8 size;
	tGRP value;
}tsSV_GRP;
typedef tsSV_GRP _SV_MEM_ *tspSV_GRP;



#endif

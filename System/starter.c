/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     starter.c
 * @brief    启动任务
 * @author   迟凯峰
 * @version  V2.3.0
 * @date     2023.04.25
 ******************************************************************************/

#include "syslink.h"

uCreateTask(Starter, SYSCFG_TASKPRIORITY - 2, __STACKSIZE_STARTER__, 0, 0)
{
	start_hook();
	uDeleteTasking;
	uEndTasking;
}

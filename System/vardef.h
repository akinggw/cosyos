/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     vardef.h
 * @brief    定义基本数据类型
 * @author   迟凯峰
 * @version  V2.3.0
 * @date     2023.04.25
 ******************************************************************************/

#ifndef __VARDEF_H
#define __VARDEF_H

#ifndef s8
typedef char  s8;
#endif

#ifndef s16
typedef short s16;
#endif

#ifndef s32
typedef long  s32;
#endif

#ifndef u8
typedef unsigned char   u8;
#endif

#ifndef u16
typedef unsigned short  u16;
#endif

#ifndef u32
typedef unsigned long   u32;
#endif

#ifndef bool
typedef u8  bool;
#endif

#endif

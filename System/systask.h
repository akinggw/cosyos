/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     systask.h
 * @brief    系统任务声明
 * @author   迟凯峰
 * @version  V2.3.0
 * @date     2023.04.25
 ******************************************************************************/

#ifndef __SYSTASK_H
#define __SYSTASK_H

/* 任务管理器 */
uExternTask(Taskmgr);

/* 调试任务 */
uExternTask(Debugger);

/* 启动任务 */
uExternTask(Starter);

/* 系统空闲任务 */
uExternTask(Sysidle);



#endif

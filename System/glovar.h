/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     glovar.h
 * @brief    系统全局变量声明
 * @author   迟凯峰
 * @version  V2.3.1
 * @date     2023.05.07
 ******************************************************************************/

#ifndef __GLOVAR_H
#define __GLOVAR_H
#include "redef.h"
/* The Misc */
extern  volatile  tBIT                            vRET_f;
extern  volatile  tsAlarm             _SYS_MEM_   vAlarm;
#define sEvent_Alarm vAlarm
extern  volatile  tsFault             _SYS_MEM_   vFault;
#define sEvent_Fault vFault
/* The Soft-RTC */
#if SYSCFG_SOFTRTC == __ENABLED__
extern  volatile  tsEvery             _SYS_MEM_   vEvery;
#define sEvent_Every vEvery
extern            u8                  _SYS_MEM_   vRealTime[7];
extern            u8                  _SYS_MEM_   vMonth2Date;
extern  const     u8                _CONST_MEM_   cMonthDate[13];
#endif
/* The Task */
extern  volatile  tBIT                            vScheduling_f;
extern            tTimeSharing        _SYS_MEM_   vTickCounter;
extern            tspTaskHandle       _SYS_MEM_   vACTBUF;
extern            tspTaskNode         _SYS_MEM_   vTASKING;
extern            tspTaskNode         _SYS_MEM_   vTPLTAIL;
extern  volatile  bool               _TASK_MEM_   vOVERTIME   [OS_TASKTOTAL + 1];
extern            tDelay             _TASK_MEM_   vDELAY_STMR [OS_TASKTOTAL + 1];
#if SYSCFG_TIMESHARINGMODE == 2
extern  const     tTimeSharing      _CONST_MEM_   vTIMESHARING[SYSCFG_TASKPRIORITY];
#endif
/* The Timing Interrupt-Task/Hook */
#if OS_TIMINTTOTAL
extern            bool             _TIMINT_MEM_   vTIMINT_ARLD[OS_TIMINTTOTAL];
extern            bool             _TIMINT_MEM_   vTIMINT_TYPE[OS_TIMINTTOTAL];
extern            tspTimIntHandle  _TIMINT_MEM_   vTIMINT_THDL[OS_TIMINTTOTAL];
extern            tTimInt          _TIMINT_MEM_   vTIMINT_STMR[OS_TIMINTTOTAL];
extern            tTimInt          _TIMINT_MEM_   vTIMINT_BUFF[OS_TIMINTTOTAL];
#endif
/* The Timing Query-Task/Hook */
#if OS_TIMQRYTOTAL
extern            bool             _TIMQRY_MEM_   vTIMQRY_ARLD[OS_TIMQRYTOTAL];
extern            bool             _TIMQRY_MEM_   vTIMQRY_TYPE[OS_TIMQRYTOTAL];
extern            tspTimQryHandle  _TIMQRY_MEM_   vTIMQRY_THDL[OS_TIMQRYTOTAL];
extern            tTimQry          _TIMQRY_MEM_   vTIMQRY_STMR[OS_TIMQRYTOTAL];
extern            tTimQry          _TIMQRY_MEM_   vTIMQRY_BUFF[OS_TIMQRYTOTAL];
#endif
/* The Global Variable */
extern            tspVarNode          _SYS_MEM_   vVarNode;
#if MCUCFG_MCULEVEL == __FEIPAOZI__
extern  volatile  tBIT  os_sign_write_var_write;
#define OS_VAR_WRITE   (os_sign_write_var_write ? NULL : vVarNode)
#else
#define OS_VAR_WRITE    vVarNode
#endif
/* The Message Mailbox */
#if SYSCFG_MAILBOX == __ENABLED__
extern            tspMbxNode          _SYS_MEM_   vMbxNode;
#if MCUCFG_MCULEVEL == __FEIPAOZI__
extern  volatile  tBIT  os_sign_write_mbx_write;
#define OS_MBX_WRITE   (os_sign_write_mbx_write ? NULL : vMbxNode)
#else
#define OS_MBX_WRITE    vMbxNode
#endif
#endif
/* The Direct Message */
#if SYSCFG_DIRMSG == __ENABLED__
extern  volatile  tBIT                            vSendDM_f;
#endif
/* The Debug Interface */
extern            char                _DEBUG_LMEM_  vDebugRecvBuff[OS_DEBUGRECVBUFFSIZE];
extern            char _DEBUG_LMEM_ * _DEBUG_HMEM_  vDebugRecvPtr;
extern  volatile  u8                  _DEBUG_HMEM_  vDebugSendFlag;
#define CmdlineSendFlag  0x01
#define TaskmgrSendFlag  0x02
extern            char _DEBUG_LMEM_ * _DEBUG_HMEM_  vDebugSendPtr;
extern            char                _DEBUG_LMEM_  vCmdLineSendBuff[OS_CMDLINESENDBUFFSIZE];
extern            char                _DEBUG_LMEM_  vTaskmgrSendBuff[OS_TASKMGRSENDBUFFSIZE];
extern  volatile  bool                _DEBUG_HMEM_  vTaskmgrBinary;
#if SYSCFG_STKTIMECOUNT == __ENABLED__
extern            u32                 _DEBUG_HMEM_  vSTK_Counter1;
extern            u32                 _DEBUG_HMEM_  vSTK_Counter2;
#endif
#if SYSCFG_TASKPCMONITOR == __ENABLED__
extern            tPC                 _DEBUG_HMEM_  vPC;
#endif
/* The Task Critical Counter */
#if SYSCFG_TASKCRITICAL == __ENABLED__
extern  volatile  u8                  _SYS_MEM_   vTaskCritical;
#endif
/* The Global Critical Counter */
#if SYSCFG_GLOBALCRITICAL == __ENABLED__
extern  volatile  u8                  _SYS_MEM_   vGlobalCritical;
#endif

#endif

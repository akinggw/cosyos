/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     syshook.h
 * @brief    ϵͳ���Ӻ�������
 * @author   �ٿ���
 * @version  V2.3.0
 * @date     2023.04.25
 ******************************************************************************/

#ifndef __SYSHOOK_H
#define __SYSHOOK_H

/* ��ʼ������ */
extern void init_hook(void);

/* �������� */
extern void start_hook(void);

/* �δ��� */
extern void tick_hook(void);

/* ���й��� */
extern void idle_hook(void);



#endif

/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     glovar.c
 * @brief    系统全局变量定义
 * @author   迟凯峰
 * @version  V2.3.1
 * @date     2023.05.07
 ******************************************************************************/

#include "redef.h"
/* The Misc */
volatile  tBIT                            vRET_f = true;
volatile  tsAlarm             _SYS_MEM_   vAlarm = {false};
volatile  tsFault             _SYS_MEM_   vFault = {false};
/* The Soft-RTC */
#if SYSCFG_SOFTRTC == __ENABLED__
volatile  tsEvery             _SYS_MEM_   vEvery = {false};
          u8                  _SYS_MEM_   vRealTime[7] = sInitRealTime;
          u8                  _SYS_MEM_   vMonth2Date  = 28;
const     u8                _CONST_MEM_   cMonthDate[13] = {31,31,28,31,30,31,30,31,31,30,31,30,31};
#endif
/* The Task */
volatile  tBIT                            vScheduling_f = false;
          tTimeSharing        _SYS_MEM_   vTickCounter = 0;
          tspTaskHandle       _SYS_MEM_   vACTBUF = (tspTaskHandle)0;
          tspTaskNode         _SYS_MEM_   vTASKING = NULL;
          tspTaskNode         _SYS_MEM_   vTPLTAIL = NULL;
volatile  bool               _TASK_MEM_   vOVERTIME   [OS_TASKTOTAL + 1];
          tDelay             _TASK_MEM_   vDELAY_STMR [OS_TASKTOTAL + 1];
#if SYSCFG_TIMESHARINGMODE == 2
const     tTimeSharing      _CONST_MEM_   vTIMESHARING[SYSCFG_TASKPRIORITY] = {SYSCFG_CUSTOMTIMESHARING};
#endif
/* The Timing Interrupt-Task/Hook */
#if OS_TIMINTTOTAL
          bool             _TIMINT_MEM_   vTIMINT_ARLD[OS_TIMINTTOTAL];
          bool             _TIMINT_MEM_   vTIMINT_TYPE[OS_TIMINTTOTAL];
          tspTimIntHandle  _TIMINT_MEM_   vTIMINT_THDL[OS_TIMINTTOTAL];
          tTimInt          _TIMINT_MEM_   vTIMINT_STMR[OS_TIMINTTOTAL];
          tTimInt          _TIMINT_MEM_   vTIMINT_BUFF[OS_TIMINTTOTAL];
#endif
/* The Timing Query-Task/Hook */
#if OS_TIMQRYTOTAL
          bool             _TIMQRY_MEM_   vTIMQRY_ARLD[OS_TIMQRYTOTAL];
          bool             _TIMQRY_MEM_   vTIMQRY_TYPE[OS_TIMQRYTOTAL];
          tspTimQryHandle  _TIMQRY_MEM_   vTIMQRY_THDL[OS_TIMQRYTOTAL];
          tTimQry          _TIMQRY_MEM_   vTIMQRY_STMR[OS_TIMQRYTOTAL];
          tTimQry          _TIMQRY_MEM_   vTIMQRY_BUFF[OS_TIMQRYTOTAL];
#endif
/* The Global Variable */
          tspVarNode          _SYS_MEM_   vVarNode = NULL;
#if MCUCFG_MCULEVEL == __FEIPAOZI__
volatile  tBIT                            os_sign_write_var_write = false;
#endif
/* The Message Mailbox */
#if SYSCFG_MAILBOX == __ENABLED__
          tspMbxNode          _SYS_MEM_   vMbxNode = NULL;
#if MCUCFG_MCULEVEL == __FEIPAOZI__
volatile  tBIT                            os_sign_write_mbx_write = false;
#endif
#endif
/* The Direct Message */
#if SYSCFG_DIRMSG == __ENABLED__
volatile  tBIT                            vSendDM_f = false;
#endif
/* The Debug Interface */
          char                _DEBUG_LMEM_  vDebugRecvBuff[OS_DEBUGRECVBUFFSIZE];
          char _DEBUG_LMEM_ * _DEBUG_HMEM_  vDebugRecvPtr = vDebugRecvBuff;
volatile  u8                  _DEBUG_HMEM_  vDebugSendFlag = 0;
          char _DEBUG_LMEM_ * _DEBUG_HMEM_  vDebugSendPtr;
          char                _DEBUG_LMEM_  vCmdLineSendBuff[OS_CMDLINESENDBUFFSIZE];
          char                _DEBUG_LMEM_  vTaskmgrSendBuff[OS_TASKMGRSENDBUFFSIZE];
volatile  bool                _DEBUG_HMEM_  vTaskmgrBinary = SYSCFG_TASKMGRAUTOSTART;
#if SYSCFG_STKTIMECOUNT == __ENABLED__
          u32                 _DEBUG_HMEM_  vSTK_Counter1 = 0;
          u32                 _DEBUG_HMEM_  vSTK_Counter2 = 0;
#endif
#if SYSCFG_TASKPCMONITOR == __ENABLED__
          tPC                 _DEBUG_HMEM_  vPC = 0;
#endif
/* The Task Critical Counter */
#if SYSCFG_TASKCRITICAL == __ENABLED__
volatile  u8                  _SYS_MEM_   vTaskCritical = 0;
#endif
/* The Global Critical Counter */
#if SYSCFG_GLOBALCRITICAL == __ENABLED__
volatile  u8                  _SYS_MEM_   vGlobalCritical = 0;
#endif

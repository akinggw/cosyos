/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     usrapi.h
 * @brief    API for User
 * @author   迟凯峰
 * @version  V2.3.4
 * @date     2023.05.07
 ******************************************************************************/

#ifndef __USRAPI_H
#define __USRAPI_H

/*
 * 声明任务
 */

/* 无私信 */
#define uExternTask(task)              sCSV_ExternTask(task)

/* 有私信 */
/* [C89] */                            #if SYSCFG_COMPILEMODE == __C89__
#define uExternTask_DM(task)/*<...)>*/ sCSV_ExternTask_DM(task)
/* [C99] */                            #elif SYSCFG_COMPILEMODE == __C99__
#define uExternTask_DM(task, ...)      sCSV_ExternTask_DM(task, __VA_ARGS__)
                                       #endif


/*
 * 创建任务
 */

/* 创建一般任务 */
#define uCreateTask(task, tpl, tss, srt, ndm)/*[(...)]<{任务代码}>*/                          sCSV_CreateTask(task, tpl, tss, srt, ndm)

/* 创建定时中断任务 */
#define uCreateTask_TimInt(tmid, arl, task, tpl, tss, srt, ndm)/*[(...)]<{任务代码}>*/        sCSV_CreateTask_TimInt(tmid, arl, task, tpl, tss, srt, ndm)

/* 创建定时查询任务 */
#define uCreateTask_TimQry(tmid, event, arl, task, tpl, tss, srt, ndm)/*[(...)]<{任务代码}>*/ sCSV_CreateTask_TimQry(tmid, event, arl, task, tpl, tss, srt, ndm)



/*
 * 创建钩子
 */

/* 创建定时中断钩子 */
#define uCreateHook_TimInt(tmid, arl, hook)/*<{钩子代码}>*/        sCSV_CreateHook_TimInt(tmid, arl, hook)

/* 创建定时查询钩子 */
#define uCreateHook_TimQry(tmid, event, arl, hook)/*<{钩子代码}>*/ sCSV_CreateHook_TimQry(tmid, event, arl, hook)



/*
 * 启动任务
 */

/* 启动任务 */
#define uStartTask(task, state)        sUSV_StartTask(&vTaskHandle_##task, state)

/* 启动任务并就绪 */
#define uStartTask_Ready(task)         sUSV_StartTask(&vTaskHandle_##task, __READY__)

/* 启动任务并挂起 */
#define uStartTask_Suspend(task)       sUSV_StartTask(&vTaskHandle_##task, __SUSPENDED__)



/*
 * 操作任务
 */

/* 恢复任务 */
#define uResumeTask(task)              sUSV_ResumeTask(sTaskNode(&vTaskHandle_##task))
#define tResumeTask(task)              sTSV_ResumeTask(sTaskNode(&vTaskHandle_##task))
/* [ARM] */                            #if MCUCFG_MCUARC == __ARM__
#define iResumeTask(task)              sISV_ResumeTask(sTaskNode(&vTaskHandle_##task), 0x00)
/* [!ARM] */                           #else
#define iResumeTask(task, svid)        sISV_ResumeTask(sTaskNode(&vTaskHandle_##task), svid)
                                       #endif
/* 挂起任务 */
#define uSuspendTask(task)             sUSV_SuspendTask(sTaskNode(&vTaskHandle_##task))
#define tSuspendTask(task)             sTSV_SuspendTask(sTaskNode(&vTaskHandle_##task))
/* [ARM] */                            #if MCUCFG_MCUARC == __ARM__
#define iSuspendTask(task)             sISV_SuspendTask(sTaskNode(&vTaskHandle_##task), 0x00)
/* [!ARM] */                           #else
#define iSuspendTask(task, svid)       sISV_SuspendTask(sTaskNode(&vTaskHandle_##task), svid)
                                       #endif
/* 删除任务 */
#define uDeleteTask(task)              sUSV_DeleteTask(sTaskNode(&vTaskHandle_##task))
#define tDeleteTask(task)              sTSV_DeleteTask(sTaskNode(&vTaskHandle_##task))
/* [ARM] */                            #if MCUCFG_MCUARC == __ARM__
#define iDeleteTask(task)              sISV_DeleteTask(sTaskNode(&vTaskHandle_##task), 0x00)
/* [!ARM] */                           #else
#define iDeleteTask(task, svid)        sISV_DeleteTask(sTaskNode(&vTaskHandle_##task), svid)
                                       #endif
/* 设置任务优先级 */
#define uTaskPriority(task, tpl)       sUSV_SetPriority(sTaskNode(&vTaskHandle_##task), tpl)
#define tTaskPriority(task, tpl)       sTSV_SetPriority(sTaskNode(&vTaskHandle_##task), tpl)
/* [ARM] */                            #if MCUCFG_MCUARC == __ARM__
#define iTaskPriority(task, tpl)       sISV_SetPriority(sTaskNode(&vTaskHandle_##task), tpl, 0x00)
/* [!ARM] */                           #else
#define iTaskPriority(task, tpl, svid) sISV_SetPriority(sTaskNode(&vTaskHandle_##task), tpl, svid)
                                       #endif


/*
 * 操作自身任务
 */

/* 设置自身任务优先级 */
#define uTaskingPriority(tpl)   sUSV_SetPriority(vTASKING, tpl)

/* 挂起自身任务 */
#define uSuspendTasking         sUSV_SuspendTask(vTASKING)

/* 删除自身任务 */
#define uDeleteTasking          sUSV_DeleteTask(vTASKING)

/* 恢复指定任务并挂起自身任务 */
#define uResumeSuspend(task)    sUSV_ResSusTask(sTaskNode(&vTaskHandle_##task))

/* 结束自身任务 */
#define uEndTasking             }while(true)



/*
 * 私信
 */

/* 接收私信 */
#define /*|res|*/uRecvDM(tc)    sUSV_RecvDM(tc)

/* 发送私信 */
/* [C89] */                     #if SYSCFG_COMPILEMODE == __C89__
#define uSendDM(task)/*<...)>*/ sUSV_SendDM(task)
#define tSendDM(task)/*<...)>*/ sTSV_SendDM(task)
/* [C99] */                     #elif SYSCFG_COMPILEMODE == __C99__
#define uSendDM(task, ...)      sUSV_SendDM(task, __VA_ARGS__)
#define tSendDM(task, ...)      sTSV_SendDM(task, __VA_ARGS__)
                                #endif


/*
 * 全局变量访问
 */

/* 读全局变量（滴答中直接读，任务中也可先进入任务临界区而后再读再退出任务临界区）*/
#define /*|gv|*/uReadGVar(gv)           sUSV_ReadGVar(gv)
/* [GNU] */                             #if SYSCFG_GNUEXT == __ENABLED__
#define /*|gv|*/iReadGVar(gv)           sISV_ReadGVar(gv, typeof(gv))
/* [!GNU] */                            #else
#define /*|gv|*/iReadGVar(gv, type)     sISV_ReadGVar(gv, type)
                                        #endif
/* 读全局数组（滴答中直接memcpy，任务中也可先进入任务临界区而后再memcpy再退出任务临界区）*/
#define         uReadGAry(lp, gp, size) sUSV_ReadGAry(lp, gp, size)
#define /*|lp|*/iReadGAry(lp, gp, size) sISV_ReadGAry(lp, gp, size)

/* 读全局字符串（滴答中直接strcpy，任务中也可先进入任务临界区而后再strcpy再退出任务临界区）*/
#define         uReadGStr(ls, gs)       sUSV_ReadGStr(ls, gs)
#define /*|ls|*/iReadGStr(ls, gs)       sISV_ReadGStr(ls, gs)

/* 写全局变量 */
#define uWriteGVar(gv, lp)              sUSV_WriteGVar(&gv, lp, sizeof(gv))
#define tWriteGVar(gv, lp)              sTSV_WriteGVar(&gv, lp, sizeof(gv))
/* [ARM] */                             #if MCUCFG_MCUARC == __ARM__
#define iWriteGVar(gv, lp)              sISV_WriteGVar(&gv, lp, sizeof(gv), 0x00)
/* [!ARM] */                            #else
#define iWriteGVar(gv, lp, svid)        sISV_WriteGVar(&gv, lp, sizeof(gv), svid)
                                        #endif
/* 写全局数组 */
#define uWriteGAry(gp, lp, size)        sUSV_WriteGVar(gp, lp, size)
#define tWriteGAry(gp, lp, size)        sTSV_WriteGVar(gp, lp, size)
/* [ARM] */                             #if MCUCFG_MCUARC == __ARM__
#define iWriteGAry(gp, lp, size)        sISV_WriteGVar(gp, lp, size, 0x00)
/* [!ARM] */                            #else
#define iWriteGAry(gp, lp, size, svid)  sISV_WriteGVar(gp, lp, size, svid)
                                        #endif
/* 写全局字符串 */
#define uWriteGStr(gs, ls)              sUSV_WriteGVar(gs, ls, 0)
#define tWriteGStr(gs, ls)              sTSV_WriteGVar(gs, ls, 0)
/* [ARM] */                             #if MCUCFG_MCUARC == __ARM__
#define iWriteGStr(gs, ls)              sISV_WriteGVar(gs, ls, 0, 0x00)
/* [!ARM] */                            #else
#define iWriteGStr(gs, ls, svid)        sISV_WriteGVar(gs, ls, 0, svid)
                                        #endif
/* 全局变量自运算 */
/* [GNU] */                             #if SYSCFG_GNUEXT == __ENABLED__
#define uSelfOpe(gv, fp)                sUSV_SelfOpe(gv, typeof(gv), fp)
/* [!GNU] */                            #else
#define uSelfOpe(gv, type, fp)          sUSV_SelfOpe(gv, type, fp)
                                        #endif
/* [GNU] */                             #if SYSCFG_GNUEXT == __ENABLED__
#define tSelfOpe(gv, fp)                sTSV_SelfOpe(gv, typeof(gv), fp)
/* [!GNU] */                            #else
#define tSelfOpe(gv, type, fp)          sTSV_SelfOpe(gv, type, fp)
                                        #endif
/* [ARM] */                             #if MCUCFG_MCUARC == __ARM__
#define iSelfOpe(gv, fp)                sISV_SelfOpe(gv, typeof(gv), fp, 0x00)
/* [!ARM] */                            #else
/* [GNU] */                             #if SYSCFG_GNUEXT == __ENABLED__
#define iSelfOpe(gv, fp, svid)          sISV_SelfOpe(gv, typeof(gv), fp, svid)
/* [!GNU] */                            #else
#define iSelfOpe(gv, type, fp, svid)    sISV_SelfOpe(gv, type, fp, svid)
                                        #endif
                                        #endif

/*
 * 消息邮箱
 */

/* 声明 */
#define uExternMailbox(mbox, type)         sCSV_ExternMailbox(mbox, type)

/* 创建 */
#define uCreateMailbox(mbox, type)         sCSV_CreateMailbox(mbox, type)

/* 接收邮件 */
#define /*|res|*/uRecvMail(mail, mbox, tc) sUSV_RecvMail(mail, mbox, tc)
#define /*|res|*/tRecvMail(mail, mbox)     sTSV_RecvMail(mail, mbox)
#define /*|res|*/iRecvMail(mail, mbox)     sISV_RecvMail(mail, mbox)

/* 发送邮件 */
#define uSendMail(mbox, mail)              sUSV_SendMail(mbox, mail)
#define tSendMail(mbox, mail)              sTSV_SendMail(mbox, mail)
/* [ARM] */                                #if MCUCFG_MCUARC == __ARM__
#define iSendMail(mbox, mail)              sISV_SendMail(mbox, mail, 0x00)
/* [!ARM] */                               #else
#define iSendMail(mbox, mail, svid)        sISV_SendMail(mbox, mail, svid)
                                           #endif


/*
 * 消息队列
 */

/* 声明静态消息队列 */
#define uExternMsgQueue_Static(que)             sCSV_ExternMsgQueue_Static(que)

/* 声明动态消息队列 */
#define uExternMsgQueue_Dynamic(que)            sCSV_ExternMsgQueue_Dynamic(que)

/* 创建静态消息队列 */
#define uCreateMsgQueue_Static(que, mode, len)  sCSV_CreateMsgQueue_Static(que, mode, len)

/* 创建动态消息队列 */
#define uCreateMsgQueue_Dynamic(que, mode, len) sCSV_CreateMsgQueue_Dynamic(que, mode, len)

/* 发送消息 */
#define /*|res|*/uSendMsg(que, msg)             sUSV_SendMsg((tspMsgQueue)&que, msg)
#define /*|res|*/tSendMsg(que, msg)             sTSV_SendMsg((tspMsgQueue)&que, msg)
/* [ARM] */                                     #if MCUCFG_MCUARC == __ARM__
#define iSendMsg(que, msg)                      sISV_SendMsg((tspMsgQueue)&que, msg, 0x00)
/* [!ARM] */                                    #else
#define iSendMsg(que, msg, svid)                sISV_SendMsg((tspMsgQueue)&que, msg, svid)
                                                #endif
/* 接收消息 */
#define /*|消息指针|*/uRecvMsg(que, tc)         sUSV_RecvMsg((tspMsgQueue)&que, tc)
#define /*|消息指针|*/tRecvMsg(que)             sTSV_RecvMsg((tspMsgQueue)&que)
#define /*|消息指针|*/iRecvMsg(que)             sISV_RecvMsg((tspMsgQueue)&que)



/*
 * 二值信号量
 */

/* 声明 */
#define uExternBin(bin)             sCSV_ExternBin(bin)

/* 创建 */
#define uCreateBin(bin)/*[= init]*/ sCSV_CreateBin(bin)

/* 等待 */
#define /*|res|*/uWaitBin(bin, tc)  sUSV_WaitBin(bin, tc)

/* 获取 */
#define /*|res|*/uTakeBin(bin, tc)  sUSV_TakeBin(bin, tc)
#define /*|res|*/tTakeBin(bin)      sTSV_TakeBin(bin)
#define /*|res|*/iTakeBin(bin)      sISV_TakeBin(bin)

/* 查询（直接查询即可）*/

/* 释放（也可直接写true）*/
#define uFreeBin(bin) bin = true
#define tFreeBin(bin) bin = true
#define iFreeBin(bin) bin = true

/* 上锁（也可直接写false）*/
#define uLockBin(bin) bin = false
#define tLockBin(bin) bin = false
#define iLockBin(bin) bin = false



/*
 * 互斥信号量
 */

/* 声明 */
#define uExternMut(mut) sCSV_ExternMut(mut)

/* 创建 */
#define uCreateMut(mut) sCSV_CreateMut(mut)

/* 释放 */
#define uFreeMut(mut)   sUSV_FreeMut(mut)

/* 获取 */
#define /*|res|*/uTakeMut(mut, tc) sUSV_TakeMut(mut, tc)



/*
 * 计数信号量
 */

/* 声明 */
#define uExternSem(sem)            sCSV_ExternSem(sem)

/* 创建 */
#define uCreateSem(sem, init, max) sCSV_CreateSem(sem, init, max)

/* 获取（中断中不可获取）*/
#define /*|res|*/uTakeSem(sem, tc) sUSV_TakeSem(sem, tc)
#define /*|res|*/tTakeSem(sem)     sTSV_TakeSem(sem)

/* 释放 */
#define uFreeSem(sem)              sUSV_FreeSem(sem)
#define tFreeSem(sem)              sTSV_FreeSem(sem)
/* [ARM] */                        #if MCUCFG_MCUARC == __ARM__
#define iFreeSem(sem)              sISV_FreeSem(sem, 0x00)
/* [!ARM] */                       #else
#define iFreeSem(sem, svid)        sISV_FreeSem(sem, svid)
                                   #endif


/*
 * 延时
 */

/* 延时滴答周期 */
#define uDelay_tc(tc) sUSV_Delay(tc)

/* 延时毫秒 */
#define uDelay_ms(ms) uDelay_tc((1000UL * ms) / SYSCFG_STKCYCLE)

/* 延时秒钟 */
#define uDelay_s(s)   uDelay_ms(1000UL * s)

/* 延时分钟 */
#define uDelay_m(m)   uDelay_s(60UL * m)

/* 延时小时 */
#define uDelay_h(h)   uDelay_m(60UL * h)



/*
 * 定时中断
 */

/* 定时中断滴答周期 */
#define uTimInt_tc(tmid, tc)       sUSV_TimInt(tmid, tc)
#define tTimInt_tc(tmid, tc)       sTSV_TimInt(tmid, tc)
/* [ARM] */                        #if MCUCFG_MCUARC == __ARM__
#define iTimInt_tc(tmid, tc)       sISV_TimInt(tmid, tc, 0x00)
/* [!ARM] */                       #else
#define iTimInt_tc(tmid, tc, svid) sISV_TimInt(tmid, tc, svid)
                                   #endif
/* 定时中断毫秒 */
#define uTimInt_ms(tmid, ms)       uTimInt_tc(tmid, (1000UL * ms) / SYSCFG_STKCYCLE)
#define tTimInt_ms(tmid, ms)       tTimInt_tc(tmid, (1000UL * ms) / SYSCFG_STKCYCLE)
/* [ARM] */                        #if MCUCFG_MCUARC == __ARM__
#define iTimInt_ms(tmid, ms)       iTimInt_tc(tmid, (1000UL * ms) / SYSCFG_STKCYCLE)
/* [!ARM] */                       #else
#define iTimInt_ms(tmid, ms, svid) iTimInt_tc(tmid, (1000UL * ms) / SYSCFG_STKCYCLE, svid)
                                   #endif
/* 定时中断秒钟 */
#define uTimInt_s(tmid, s)         uTimInt_ms(tmid, 1000UL * s)
#define tTimInt_s(tmid, s)         tTimInt_ms(tmid, 1000UL * s)
/* [ARM] */                        #if MCUCFG_MCUARC == __ARM__
#define iTimInt_s(tmid, s)         iTimInt_ms(tmid, 1000UL * s)
/* [!ARM] */                       #else
#define iTimInt_s(tmid, s, svid)   iTimInt_ms(tmid, 1000UL * s, svid)
                                   #endif
/* 定时中断分钟 */
#define uTimInt_m(tmid, m)         uTimInt_s(tmid, 60UL * m)
#define tTimInt_m(tmid, m)         tTimInt_s(tmid, 60UL * m)
/* [ARM] */                        #if MCUCFG_MCUARC == __ARM__
#define iTimInt_m(tmid, m)         iTimInt_s(tmid, 60UL * m)
/* [!ARM] */                       #else
#define iTimInt_m(tmid, m, svid)   iTimInt_s(tmid, 60UL * m, svid)
                                   #endif
/* 定时中断小时 */
#define uTimInt_h(tmid, h)         uTimInt_m(tmid, 60UL * h)
#define tTimInt_h(tmid, h)         tTimInt_m(tmid, 60UL * h)
/* [ARM] */                        #if MCUCFG_MCUARC == __ARM__
#define iTimInt_h(tmid, h)         iTimInt_m(tmid, 60UL * h)
/* [!ARM] */                       #else
#define iTimInt_h(tmid, h, svid)   iTimInt_m(tmid, 60UL * h, svid)
                                   #endif
/* 定时中断定时器自动重装载 */
#define uTimInt_AutoReload(tmid, arl)  vTIMINT_ARLD[tmid] = arl
#define tTimInt_AutoReload(tmid, arl)  vTIMINT_ARLD[tmid] = arl
#define iTimInt_AutoReload(tmid, arl)  vTIMINT_ARLD[tmid] = arl



/*
 * 定时查询
 */

/* 定时查询滴答周期 */
#define uTimQry_tc(tmid, tc)       sUSV_TimQry(tmid, tc)
#define tTimQry_tc(tmid, tc)       sTSV_TimQry(tmid, tc)
/* [ARM] */                        #if MCUCFG_MCUARC == __ARM__
#define iTimQry_tc(tmid, tc)       sISV_TimQry(tmid, tc, 0x00)
/* [!ARM] */                       #else
#define iTimQry_tc(tmid, tc, svid) sISV_TimQry(tmid, tc, svid)
                                   #endif
/* 定时查询毫秒 */
#define uTimQry_ms(tmid, ms)       uTimQry_tc(tmid, (1000UL * ms) / SYSCFG_STKCYCLE)
#define tTimQry_ms(tmid, ms)       tTimQry_tc(tmid, (1000UL * ms) / SYSCFG_STKCYCLE)
/* [ARM] */                        #if MCUCFG_MCUARC == __ARM__
#define iTimQry_ms(tmid, ms)       iTimQry_tc(tmid, (1000UL * ms) / SYSCFG_STKCYCLE)
/* [!ARM] */                       #else
#define iTimQry_ms(tmid, ms, svid) iTimQry_tc(tmid, (1000UL * ms) / SYSCFG_STKCYCLE, svid)
                                   #endif
/* 定时查询秒钟 */
#define uTimQry_s(tmid, s)         uTimQry_ms(tmid, 1000UL * s)
#define tTimQry_s(tmid, s)         tTimQry_ms(tmid, 1000UL * s)
/* [ARM] */                        #if MCUCFG_MCUARC == __ARM__
#define iTimQry_s(tmid, s)         iTimQry_ms(tmid, 1000UL * s)
/* [!ARM] */                       #else
#define iTimQry_s(tmid, s, svid)   iTimQry_ms(tmid, 1000UL * s, svid)
                                   #endif
/* 定时查询分钟 */
#define uTimQry_m(tmid, m)         uTimQry_s(tmid, 60UL * m)
#define tTimQry_m(tmid, m)         tTimQry_s(tmid, 60UL * m)
/* [ARM] */                        #if MCUCFG_MCUARC == __ARM__
#define iTimQry_m(tmid, m)         iTimQry_s(tmid, 60UL * m)
/* [!ARM] */                       #else
#define iTimQry_m(tmid, m, svid)   iTimQry_s(tmid, 60UL * m, svid)
                                   #endif
/* 定时查询小时 */
#define uTimQry_h(tmid, h)         uTimQry_m(tmid, 60UL * h)
#define tTimQry_h(tmid, h)         tTimQry_m(tmid, 60UL * h)
/* [ARM] */                        #if MCUCFG_MCUARC == __ARM__
#define iTimQry_h(tmid, h)         iTimQry_m(tmid, 60UL * h)
/* [!ARM] */                       #else
#define iTimQry_h(tmid, h, svid)   iTimQry_m(tmid, 60UL * h, svid)
                                   #endif
/* 定时查询定时器自动重装载 */
#define uTimQry_AutoReload(tmid, arl)  vTIMQRY_ARLD[tmid] = arl
#define tTimQry_AutoReload(tmid, arl)  vTIMQRY_ARLD[tmid] = arl
#define iTimQry_AutoReload(tmid, arl)  vTIMQRY_ARLD[tmid] = arl



/*
 * 软件RTC
 */

/* 设置时间 */
#define uSetTime(t)       uWriteGAry(&vRealTime, t, sizeof(vRealTime))
#define tSetTime(t)       tWriteGAry(&vRealTime, t, sizeof(vRealTime))
/* [ARM] */               #if MCUCFG_MCUARC == __ARM__
#define iSetTime(t)       iWriteGAry(&vRealTime, t, sizeof(vRealTime), 0x00)
/* [!ARM] */              #else
#define iSetTime(t, svid) iWriteGAry(&vRealTime, t, sizeof(vRealTime), svid)
                          #endif
/* 获取时间 */
#define uGetTime(t)       uReadGAry(t, &vRealTime, sizeof(vRealTime))
#define tGetTime(t)        __memcpy(t, &vRealTime, sizeof(vRealTime))
#define iGetTime(t)       iReadGAry(t, &vRealTime, sizeof(vRealTime))



/*
 * 进程内存
 */

/* malloc */
#define /*|ptr|*/uMalloc(size)        sUSV_Malloc(size)

/* calloc */
#define /*|ptr|*/uCalloc(nmemb, size) sUSV_Calloc(nmemb, size)

/* realloc */
#define /*|ptr|*/uRealloc(p, size)    sUSV_Realloc(p, size)

/* free */
#define uFree(p)                      sUSV_Free(p)



/*
 * 线程内存
 */

/* 创建线程内存池 */
#define uCreateMempool              sCSV_CreateMempool

/* 初始化线程内存池 */
#define /*|res|*/uInitMempool(size) sUSV_InitMempool(size)

/* 线程内存分配 */
#define /*|ptr|*/uTalloc(size)      sUSV_Talloc(size)

/* 释放线程内存池 */
#define uFreeMempool                sUSV_FreeMempool



/*
 * 事件标志组
 */

/* 声明标志组 */
#define uExternFlagGroup(group)/*<{...}>*/  sCSV_ExternFlagGroup(group)

/* 定义标志位 */
#define uDefFlagBit(bit)                    sDefBitField(bit)

/* 定义空位 */
#define uDefVoidBits(nvb)                   sDefVoidBits(nvb)

/* 创建标志组 */
#define uCreateFlagGroup(group)/*[={...}]*/ sCSV_CreateFlagGroup(group)

/* 查询标志组 */
#define /*|res|*/uQueryFlagGroup(group)     sUSV_QueryFlagGroup(group)
#define /*|res|*/tQueryFlagGroup(group)     sTSV_QueryFlagGroup(group)
#define /*|res|*/iQueryFlagGroup(group)     sISV_QueryFlagGroup(group)

/* 清除标志组 */
#define uClearFlagGroup(group)              sUSV_ClearFlagGroup(group)
#define tClearFlagGroup(group)              sTSV_ClearFlagGroup(group)
/* [ARM] */                                 #if MCUCFG_MCUARC == __ARM__
#define iClearFlagGroup(group)              sISV_ClearFlagGroup(group, 0x00)
/* [!ARM] */                                #else
#define iClearFlagGroup(group, svid)        sISV_ClearFlagGroup(group, svid)
                                            #endif
/* 等待标志组 */
#define /*|res|*/uWaitFlagGroup(group, tc)  sUSV_WaitFlagGroup(group, tc)

/* 查询标志位（任务、滴答、中断中，均直接查询）*/

/* 设置标志位（滴答中直接设置）*/
#define uSetFlagBit(group, bit)             sUSV_WriteFlagBit(group, bit, true)
/* [ARM] */                                 #if MCUCFG_MCUARC == __ARM__
#define iSetFlagBit(group, bit)             iSetFlagBits(group, 1) bit)
/* [!ARM] */                                #else
#define iSetFlagBit(group, bit, svid)       iSetFlagBits(group, 1, svid) bit)
                                            #endif
/* 清除标志位（滴答中直接清除）*/
#define uClearFlagBit(group, bit)               sUSV_WriteFlagBit(group, bit, false)
/* [ARM] */                                     #if MCUCFG_MCUARC == __ARM__
#define iClearFlagBit(group, bit)               iClearFlagBits(group, 1) bit)
/* [!ARM] */                                    #else
#define iClearFlagBit(group, bit, svid)         iClearFlagBits(group, 1, svid) bit)
                                                #endif
/* 设置多标志位（滴答中直接设置，任务中应先进入任务临界区而后再设置再退出任务临界区）*/
/* [ARM] */                                          #if MCUCFG_MCUARC == __ARM__
#define iSetFlagBits(group, nbit)/*<...)>*/          sISV_WriteFlagBits(group, +, nbit, 0x00)
/* [!ARM] */                                         #else
#define iSetFlagBits(group, nbit, svid)/*<...)>*/    sISV_WriteFlagBits(group, +, nbit, svid)
                                                     #endif
/* 清除多标志位（滴答中直接清除，任务中应先进入任务临界区而后再清除再退出任务临界区）*/
/* [ARM] */                                          #if MCUCFG_MCUARC == __ARM__
#define iClearFlagBits(group, nbit)/*<...)>*/        sISV_WriteFlagBits(group, -, nbit, 0x00)
/* [!ARM] */                                         #else
#define iClearFlagBits(group, nbit, svid)/*<...)>*/  sISV_WriteFlagBits(group, -, nbit, svid)
                                                     #endif


/*
 * DEBUG
 */

/* [ARM] */              #if MCUCFG_MCUARC == __ARM__
#define iDebugSend       iTimQry_ms(SYSCFG_USERTIMQRYTOTAL, 40)
#define iDebugRecv       iTimInt_ms(SYSCFG_USERTIMINTTOTAL, 20)
/* [!ARM] */             #else
#define iDebugSend(svid) iTimQry_ms(SYSCFG_USERTIMQRYTOTAL, 40, svid)
#define iDebugRecv(svid) iTimInt_ms(SYSCFG_USERTIMINTTOTAL, 20, svid)
                         #endif
#if false
/* 应用示例 - 8051/80251 UART1 */
void UART1_Isr(void) interrupt 4 using 2
{
	if(TI)
	{
		TI = 0;
		if(*vDebugSendPtr)
		{
			SBUF = *vDebugSendPtr++;
		}
		else
		{
			iDebugSend(0);
		}
	}
	if(RI)
	{
		RI = 0;
		if(vDebugRecvPtr - vDebugRecvBuff < OS_DEBUGRECVBUFFSIZE)
		{
			*vDebugRecvPtr++ = SBUF;
		}
		iDebugRecv(1);
	}
}

/* 应用示例 - STM32 USART1 */
void USART1_IRQHandler(void)
{
	if(USART_GetITStatus(USART1, USART_IT_TXE) != RESET)
	{
		if(*vDebugSendPtr)
		{
			USART_SendData(USART1, *vDebugSendPtr++);
		}
		else
		{
			USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
			iDebugSend;
		}
	}
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		if(vDebugRecvPtr - vDebugRecvBuff < OS_DEBUGRECVBUFFSIZE)
		{
			*vDebugRecvPtr++ = USART_ReceiveData(USART1);
		}
		iDebugRecv;
	}
}

#endif



/*
 * 任务临界区（任务级临界区保护，仅支持在任务中调用）
 */

/* 进入临界区 */
#if SYSCFG_TASKCRITICAL == __ENABLED__
#define uEnterCritical	\
do{	\
	if(!vTaskCritical) mEnterCritical;	\
	vTaskCritical++;	\
}while(false)
#else
#define uEnterCritical mEnterCritical
#endif

/* 退出临界区 */
#if SYSCFG_TASKCRITICAL == __ENABLED__
#define uExitCritical	\
do{	\
	if( vTaskCritical) vTaskCritical--;	\
	if(!vTaskCritical) mExitCritical;	\
}while(false)
#else
#define uExitCritical  mExitCritical
#endif



/*
 * 全局临界区（系统级临界区保护，支持在任意处调用）
 */

/* 进入临界区 */
#if SYSCFG_GLOBALCRITICAL == __ENABLED__
#define xDisableIRQ	\
do{	\
	if(!vGlobalCritical) mDisableIRQ;	\
	vGlobalCritical++;	\
}while(false)
#else
#define xDisableIRQ mDisableIRQ
#endif

/* 退出临界区 */
#if SYSCFG_GLOBALCRITICAL == __ENABLED__
#define xEnableIRQ	\
do{	\
	if( vGlobalCritical) vGlobalCritical--;	\
	if(!vGlobalCritical) mEnableIRQ;	\
}while(false)
#else
#define xEnableIRQ  mEnableIRQ
#endif



/*
 * 任务栈监控（线程入口监控）
 */

#define uEntryMonitor   mEntry_Monitor



#endif

/**************************************************************************//**
 * @item     CosyOS Kernel
 * @file     tsv.c
 * @brief    Service for SysTick_Handler(Tick-Hook, Timing Interrupt-Hook, Timing Query-Hook)
 * @author   迟凯峰
 * @version  V2.3.1
 * @date     2023.05.02
 ******************************************************************************/

#include "glovar.h"
#include "sysapi.h"
#include "svs.h"

#if SYSCFG_MSGQUEUE == __ENABLED__

bool send_msg__(tspMsgQueue msg_queue, void *msg) __C51USING__
{
	msg_queue->mutex = false;
	if(msg_queue->counter == msg_queue->len)
	{
		vAlarm.overflow_msg_queue = true;
		msg_queue->mutex = true;
		return false;
	}
	if(msg_queue->type == __DYNAMIC__)
	{
		tspMsgNode msg_node = NULL;
		msg_node = (tspMsgNode)__malloc(sizeof(tsMsgNode));
		if(msg_node == NULL)
		{
			vFault.malloc_fail_send_msg = true;
			msg_queue->mutex = true;
			return false;
		}
		sSendMsg_Dynamic(msg);
	}
	else
	{
		sSendMsg_Static(msg);
	}
	msg_queue->mutex = true;
	return true;
}

void *recv_msg__(tspMsgQueue msg_queue) __C51USING__
{
	void *msg = NULL;
	tspMsgNode msg_node;
	msg_queue->mutex = false;
	if(!msg_queue->counter);
	else if(msg_queue->type == __DYNAMIC__)
	{
		sRecvMsg_Dynamic(msg);
	}
	else
	{
		sRecvMsg_Static(msg);
	}
	msg_queue->mutex = true;
	return msg;
}

#endif

/**************************************************************************//**
 * @file     tick_hook.c
 * @brief    滴答钩子
 * @detail   每个系统滴答周期，在SysTick_Handler/定时器0中断中都会被调用一次，
             适用于每滴答周期/秒/分/时/...做一次的工作。
 * @note  1. 如果您在滴答钩子中调用API，应调用滴答API；
          2. 如果您在滴答钩子中调用自定义函数并且您的MCU内核为8051，自定义函数可能需要
             添加“__USING__”属性或声明为相对寄存器访问“#pragma NOAREGS”。
 ******************************************************************************/

#include "..\System\syslink.h"
#if SYSCFG_TICKHOOK == __ENABLED__

void tick_hook(void) __USING__
{
	/* 每滴答执行一次 */
	{
		
	}
	if(sEvent_Every.second)
	{
		sEvent_Every.second = false;
		/* 每秒钟执行一次（每秒，first tick）*/
		{
			
		}
		if(sEvent_Every.minute)
		{
			sEvent_Every.minute = false;
			/* 每分钟执行一次（每分0秒，first tick）*/
			{
				
			}
			if(sEvent_Every.hour)
			{
				sEvent_Every.hour = false;
				/* 每小时执行一次（每时0分0秒，first tick）*/
				{
					
				}
				if(sEvent_Every.day)
				{
					sEvent_Every.day = false;
					/* 每天执行一次（每天0时0分0秒，first tick）*/
					{
						
					}
					if(sEvent_Every.month)
					{
						sEvent_Every.month = false;
						/* 每月执行一次（每月1日0时0分0秒，first tick）*/
						{
							
						}
						if(sEvent_Every.year)
						{
							sEvent_Every.year = false;
							/* 每年执行一次（每年1月1日0时0分0秒，first tick）*/
							{
								
							}
						}
					}
				}
			}
		}
	}
}

#endif
